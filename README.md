# Физтех.ЧАСЫ

## Что это?

Физтех.ЧАСЫ это многофункциональное устройство с такими базовыми функциями как: часы реального времени, таймер, секндомер, будильник.

Отображение информации на экране сделано эстетично с плавными анимациями.

Подставка сменная, универсальное крепление идейно аналогично креплению "ласточкин хвост", вы можете преобразить внешний вид устройства как вам угодно.

Однако, при желании, вы можете добавить в устройство любой фунционал, который сможете придумать и запрограммировать.

## Использование по умолчанию

### Элементы управления

Взаимодействие с устройством производится:

- Переключение режимов осуществляется бесконтактным способом с помощью ультразвукового дальномера: в ближней и дальней стороне рабочего диапазона расположены "сенсорные зоны", работающие как виртуальные кнопки. Расположен на верхней стороне устройства, возможно удерживание для многократного взаимодействия.
- Настройка и запуск функций осуществляется физическим способом с помощью двух кнопок: "настройка" (отмечена шестерёнкой) и "действие" (отмечена значком play), расположены на задней стороне устройства, возможно удерживание для многократного взаимодействия.
- Дополнительно: автояркость работает при помощи двух датчиков освещения, расположенных с двух концов на верхней стороне устройства.

### Включение

Устройство питается от любого блока питания с выходом 5В 3А (до 15Вт), влючение происходит автоматически при подключении.

__Внимание__: при отключении питания информация о таймере, секндомере и будильнике теряется, сохраняется только настройка реального времени.

### Режимы работы

#### Часы

Главный режим, режим по умолчанию.

Показывает реальное время, сохраняющееся при отключении питания.

Кнопка настройки выбирает элемент для настройки (часы, минуты, секунды), кнопка действия увеличивает значение выбранного элемента

#### Таймер

Отсчитывает для вас время вплоть до 99 часов 59 минут 59 секунд. В процессе отсчёта происходит изменение основного цвета по градиенту "зелёный-жёлтый-красный".

Настройка времени для отсчёта производится аналогично режиму "Часы".

Запуск и приостановка таймера производися кнопкой действия, сброс производится кнопкой настройки.

При отсутствии взимодействия от пользователя по истечении определённого времени режим сменяется на режим по умолчанию, если таймер не запущен.

Таймер может работать в фоне, держать режим запущенным не обязательно, по истечению времени звуковой сигнал прозвучит как обычно.

#### Секундомер

Засекает для вас время до 99 часов 59 минут 59 секунд.

Запуск и приостановка секундомера производися кнопкой действия, сброс производится кнопкой настройки.

При отсутствии взимодействия от пользователя по истечении определённого времени режим сменяется на режим по умолчанию, если секундомер не запущен.

Секундомер может работать в фоне, держать режим запущенным не обязательно.

#### Будильник

Запустит вам звуковой сигнал в заданное время суток.

Настройка времени сигнала производится аналогично режиму "Часы".

Включение и выключение будильника производися кнопкой действия.

При отсутствии взимодействия от пользователя по истечении определённого времени режим сменяется на режим по умолчанию.

## Электронная начинка

Предлагается ознакомиться с начинкой проекта. Физтех.ЧАСЫ включают в себя:

- мощный микроконтроллер Raspberry Pi Pico;
- светодиодная RGB-матрица с глубиной цвета 8 бит;
- автономный модуль часов RTC, питающийся от батарейки CR2032 до 10 лет (по заявлению производителя);
- ультразвуковой дальномер, стабильно определяющий ладонь на расстоянии до 25 см;
- датчики освещения для автояркости;
- кнопки для настройки;
- динамик.
