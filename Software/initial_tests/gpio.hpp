#define HIGH 1
#define LOW 0
#define INPUT 0
#define OUTPUT 1



class Pin {
	public:
		Pin(){};
		Pin(int gpio_num);
		~Pin(){};
		int set_digital_in();
		int set_digital_out();
		int set_analog_in();
		int set_analog_out(float divider = 32.0f);
		void digital_write(bool new_state);
		bool digital_read();
		void analog_write(uint16_t new_state);
		uint16_t analog_read();
	private:
		uint8_t self_pin{0};
		bool is_set{0};
		bool digital_state{0};
		bool _digital_state{0};
		uint16_t analog_state{0};
		uint16_t _analog_state{0};
		uint8_t pin{0};
		uint slice_num{0};
		pwm_config config{0};
	protected:
		void on_pwm_wrap();
};


Pin::Pin(int gpio_num) {
	self_pin = gpio_num;
	is_set = false;
	digital_state = 0;
	_digital_state = 0;
	analog_state = 0;
	_analog_state = 0;
}

int Pin::set_digital_in() {
	if (!is_set) {
		gpio_init(self_pin);
		gpio_set_dir(self_pin, GPIO_IN);
		is_set = true;
		return 0;
	} else return 1;
}
int Pin::set_digital_out() {
	if (!is_set) {
		gpio_init(self_pin);
		gpio_set_dir(self_pin, GPIO_OUT);
		is_set = true;
		return 0;
	} else return 1;
}
int Pin::set_analog_in() {
	if (!is_set) {
		adc_gpio_init(self_pin);
		adc_layout[self_pin] = adc_next;
		++adc_next;
		is_set = true;
		return 0;
	} else return 1;
}
int Pin::set_analog_out(float divider = 32.0f) {
	if (!is_set) {
		// Tell the LED pin that the PWM is in charge of its value.
		gpio_set_function(self_pin, GPIO_FUNC_PWM);
		// Figure out which slice we just connected to the LED pin
		slice_num = pwm_gpio_to_slice_num(self_pin);
		// Mask our slice's IRQ output into the PWM block's single interrupt line,
		// and register our interrupt handler
		pwm_clear_irq(slice_num);
		pwm_set_irq_enabled(slice_num, true);
		irq_set_exclusive_handler(PWM_IRQ_WRAP, on_pwm_wrap);
	}
}



Pin pin_arrary[32];
int8_t adc_layout[32] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
int8_t adc_next = 0;
