uint8_t matrix[MATRIX_HEIGHT][MATRIX_WIDTH][3];
uint64_t cntr = 0;


uint8_t* get_screen_test() {
	for (uint32_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (uint32_t x = 0; x < MATRIX_WIDTH; ++x) {
			for (uint8_t c = 0; c < 3; ++c) {
				matrix[y][x][c] = 0;
			}
		}
	}

	matrix[3][10][1] = 255;
	matrix[3][10][2] = 127;
	matrix[6][10][0] = 127;
	matrix[6][10][2] = 255;
	
	uint64_t _cntr = cntr;
	uint8_t hour = (_cntr / 3600) % 24;
	_cntr %= 3600;
	uint8_t minute = _cntr / 60;
	uint8_t second = _cntr % 60;

	uint8_t offset_x = 0;
	uint8_t offset_y = 0;
	if (hour / 10) {
	for (uint y = 0; y < 8; ++y) {
			for (uint8_t x = 0; x < 4; ++x) {
				matrix[y+offset_y][x+offset_x][0] = 255 * nums_big[hour/10][y][x];
				matrix[y+offset_y][x+offset_x][1] = 127 * nums_big[hour/10][y][x];
				matrix[y+offset_y][x+offset_x][2] = 0 * nums_big[hour/10][y][x];
			}
		}
	}
	offset_x = 5;
	offset_y = 0;
	for (uint y = 0; y < 8; ++y) {
		for (uint8_t x = 0; x < 4; ++x) {
			matrix[y+offset_y][x+offset_x][0] = 255 * nums_big[hour%10][y][x];
			matrix[y+offset_y][x+offset_x][1] = 127 * nums_big[hour%10][y][x];
			matrix[y+offset_y][x+offset_x][2] = 0 * nums_big[hour%10][y][x];
		}
	}
	offset_x = 12;
	offset_y = 0;
	for (uint y = 0; y < 8; ++y) {
		for (uint8_t x = 0; x < 4; ++x) {
			matrix[y+offset_y][x+offset_x][0] = 255 * nums_big[minute/10][y][x];
			matrix[y+offset_y][x+offset_x][1] = 127 * nums_big[minute/10][y][x];
			matrix[y+offset_y][x+offset_x][2] = 0 * nums_big[minute/10][y][x];
		}
	}
	offset_x = 17;
	offset_y = 0;
	for (uint y = 0; y < 8; ++y) {
		for (uint8_t x = 0; x < 4; ++x) {
			matrix[y+offset_y][x+offset_x][0] = 255 * nums_big[minute%10][y][x];
			matrix[y+offset_y][x+offset_x][1] = 127 * nums_big[minute%10][y][x];
			matrix[y+offset_y][x+offset_x][2] = 0 * nums_big[minute%10][y][x];
		}
	}
	offset_x = 23;
	offset_y = 1;
	for (uint y = 0; y < 7; ++y) {
		for (uint8_t x = 0; x < 4; ++x) {
			matrix[y+offset_y][x+offset_x][0] = 255 * nums_small[second/10][y][x];
			matrix[y+offset_y][x+offset_x][1] = 127 * nums_small[second/10][y][x];
			matrix[y+offset_y][x+offset_x][2] = 0 * nums_small[second/10][y][x];
		}
	}
	offset_x = 28;
	offset_y = 1;
	for (uint y = 0; y < 7; ++y) {
		for (uint8_t x = 0; x < 4; ++x) {
			matrix[y+offset_y][x+offset_x][0] = 255 * nums_small[second%10][y][x];
			matrix[y+offset_y][x+offset_x][1] = 127 * nums_small[second%10][y][x];
			matrix[y+offset_y][x+offset_x][2] = 0 * nums_small[second%10][y][x];
		}
	}

	++cntr;
	return (uint8_t*)&matrix;
}
