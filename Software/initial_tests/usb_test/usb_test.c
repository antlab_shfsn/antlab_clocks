#include <stdio.h>
#include "pico/stdlib.h"

int main() {
    stdio_init_all();
    uint8_t val = 0;
    while (true) {
        printf("USB serial output test: %hhd\n", val);
        ++val;
        sleep_ms(10);
    }
}
