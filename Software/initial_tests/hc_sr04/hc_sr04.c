#include <stdio.h>
#include <math.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"

#ifdef PICO_DEFAULT_LED_PIN
#define LED_PIN PICO_DEFAULT_LED_PIN
#endif

#define HC_SR04_TRIGGER 7
#define HC_SR04_ECHO 6

#define dist_max 1000
#define v_sound 340


int main() {
    stdio_init_all();
#ifdef LED_PIN
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
#endif
    gpio_init(HC_SR04_TRIGGER);
    gpio_init(HC_SR04_ECHO);
    gpio_set_dir(HC_SR04_TRIGGER, GPIO_OUT);
    gpio_set_dir(HC_SR04_ECHO, GPIO_IN);
    uint64_t dist = 0;
    uint64_t delay_us_max = (dist_max << 1) * 1000 / v_sound;
    while (1) {
        uint64_t delay_us = 0;
        //printf("Pinging echo\n");
        gpio_put(LED_PIN, 1);
        gpio_put(HC_SR04_TRIGGER, 0);
        sleep_us(5);
        gpio_put(HC_SR04_TRIGGER, 1);
        sleep_us(10);
        gpio_put(HC_SR04_TRIGGER, 0);
        uint64_t echo_start = to_us_since_boot(get_absolute_time());
        while (!gpio_get(HC_SR04_ECHO)) {
            echo_start = to_us_since_boot(get_absolute_time());
        }
        while (gpio_get(HC_SR04_ECHO) && delay_us < delay_us_max) {
            delay_us = to_us_since_boot(get_absolute_time()) - echo_start;
        }
        //printf("Stop waiting for echo\n");
        gpio_put(LED_PIN, 0);
        dist = (delay_us * v_sound / 1000) >> 1;
        printf("Distamnce: %llumm, duration: %llu\n", dist, delay_us);
        sleep_ms(100);
    }
}