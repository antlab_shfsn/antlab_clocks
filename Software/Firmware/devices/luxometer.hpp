/**
 * A luxometer driver, originally created for Phystech.Clocks project.
 * 
 * Note that by default there is latch  if a `raw = true` argument is not provided.
 * Note that by default the driver returns a filtered value if a `raw = true` argument is not provided.
 * 
 * NOTE!!! This diver is designed to be externally configured. Possible additional code:
 * 
 * #include "pico/stdlib.h"
 * #include "hardware/gpio.h"
 * #include "hardware/adc.h"
 * #include ".../io.h"     // A custom console output library
 * #define DEBUG_MODE true // Set USB debug outputs on / off
 * #define LUXOMETER_PIN 27
 * #define LUXOMETER_AMP 1.0
 * #define LUXOMETER_MANUAL false    // Set manual values setting on / off
 * #define LUXOMETER_RESPONSE_TIME_MS 2000 // Time to make sure that light is changed.
 * #define LUXOMETER_UNLATCH_PERIOD_MS 30000
 * #define LUXOMETER_VALUE_TOL 16
 * #define LUXOMETER_FILTER_COEF 2500
 * #define LUXOMETER_MIN_BRT 63
 * #define LUXOMETER_GAMMA 2.7
 * io_init();       // Initialize custom debug output
 * Luxometer luxometer(LUXOMETER_PIN);  // Get a luxometer instance
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// A class that represents a single button instance
class Luxometer {
    public:
        Luxometer(uint8_t pin, float amplification);
        void set_value(uint8_t required_value);
        uint8_t get_value(bool unlatch, bool raw);
    private:
        float amp{0};
        uint8_t self_pin{0};
        uint8_t self_num{0};
        float value{0};
        float _value{0};
        float __value{0};
        float latch{0};
        uint8_t value_out{0};
        float filter{0};
        uint64_t last_time_changed{0};
        uint64_t last_time_unlatched{0};
        uint64_t latch_lag_ms{0};
};


// Constructor, initialization
Luxometer::Luxometer(uint8_t pin, float amplification) {
    amp = amplification;
    self_pin = pin;
    self_num = pin - 26;
    adc_gpio_init(self_pin);
    last_time_changed = 0;
    last_time_unlatched = 0;
    latch_lag_ms = 0;
    value = 4095.0;
    _value = 4095.0;
    __value = 4095.0;
    latch = 4095.0;
}

// Function that returns the current luxometer value
// Pass `unlatch = true` if you want values to start adapt to the environment immediately
// Pass `raw = true` if you want an unfiltered value to be returned
uint8_t Luxometer::get_value(bool unlatch = false, bool raw = false) {
    if (DEBUG_MODE && BUTTON_MANUAL) {
        print("Luxometer debug output: ");
        value_out = (uint8_t)((uint16_t)__value >> 4);
        print(value_out, '\n');
        return value_out;
    }
    adc_select_input(self_num);
    __value = (float)adc_read() * amp;
    _value = pow(__value / 4095.0, LUXOMETER_GAMMA) * 4095.0;
    if ((abs(latch - _value) > LUXOMETER_VALUE_TOL && to_ms_since_boot(get_absolute_time()) - latch_lag_ms > LUXOMETER_RESPONSE_TIME_MS)
        || unlatch || to_ms_since_boot(get_absolute_time()) - last_time_unlatched > LUXOMETER_UNLATCH_PERIOD_MS) {
        latch = _value;
        last_time_unlatched = to_ms_since_boot(get_absolute_time());
        latch_lag_ms = last_time_unlatched;
    } else if (abs(latch - _value) < LUXOMETER_VALUE_TOL) latch_lag_ms = to_ms_since_boot(get_absolute_time());
    filter = LUXOMETER_FILTER_COEF / (float)(to_ms_since_boot(get_absolute_time()) - last_time_changed);
    if (filter < 1.0) filter = 1.0;
    last_time_changed = to_ms_since_boot(get_absolute_time());
    value += ((float)latch - (float)value) / filter;
    value_out = (uint8_t)((raw ? (uint16_t)__value : (uint16_t)value) >> 4);
    if (value_out < LUXOMETER_MIN_BRT and !raw) value_out = LUXOMETER_MIN_BRT;
    return value_out;
}

// Function that manually sets luxometer value
void Luxometer::set_value(uint8_t required_value) {
    if (DEBUG_MODE && BUTTON_MANUAL) {
        print("Setting luxometer value manually to: ");
        print(required_value, '\n');
        __value = required_value;
    }
}
