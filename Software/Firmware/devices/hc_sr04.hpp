/**
 * An HC-SR04 ultrasonic rangefinder driver, originally created for Phystech.Clocks project.
 * 
 * Note that by default the driver returns a filtered range if a `raw = true` argument is not provided.
 * 
 * NOTE!!! This diver is designed to be externally configured. Possible additional code:
 * 
 * #include "pico/stdlib.h"
 * #include "hardware/gpio.h"
 * #include ".../io.h"     // A custom console output library
 * #define DEBUG_MODE true // Set USB debug outputs on / off
 * #define HC_SR04_TRIGGER 7   // Trigger pin
 * #define HC_SR04_ECHO 6      // Echo pin
 * #define MAX_DIST_MM 250
 * #define V_SOUND 340
 * #define RECAPTURE_TIME_US 1000000   // if echo is lost wait for given amount of time to recapture
 * #define HC_SR04_FILTER_COEF 30000
 * #define HC_SR04_MANUAL false    // Set manual values setting on / off
 * io_init();       // Initialize custom debug output
 * HC_SR04 rangefinder(HC_SR04_TRIGGER, HC_SR04_ECHO);  // Get a rangefinder instance
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


#define DIST_TOL_MM 5

// A class that represents a single HC_SR04 instance, i.e. range finder
class HC_SR04 {
    public:
        HC_SR04(uint8_t trigger, uint8_t echo);
        void set_range(uint64_t range);
        uint64_t get_range(bool raw);
    private:
        uint8_t self_trigger{0};
        uint8_t self_echo{0};
        uint64_t delay_us_max{0};
        uint64_t delay_us{0};
        uint64_t echo_start{0};
        bool state_active{0};
        bool _state_active{0};
        float dist{0};
        uint64_t _dist{0};
        uint64_t __dist{0};
        uint64_t last_time_measured{0};
        uint64_t last_time_reliable{0};
        uint64_t last_time_called{0};
        float filter{0};
};


// Constructor, initialization
HC_SR04::HC_SR04(uint8_t trigger, uint8_t echo) {
    self_trigger = trigger;
    self_echo = echo;
    delay_us_max = (MAX_DIST_MM << 1) * 1000 / V_SOUND;
    dist = MAX_DIST_MM;
    _dist = MAX_DIST_MM;
    __dist = MAX_DIST_MM;
    state_active = false;
    _state_active = false;
    last_time_reliable = to_us_since_boot(get_absolute_time());
    gpio_init(self_trigger);
    gpio_init(self_echo);
    gpio_set_dir(self_trigger, GPIO_OUT);
    gpio_set_dir(self_echo, GPIO_IN);
}

// Function that returns measured rangefinder distance in millimeters.
// If value returned equals MAX_DIST_MM then there's no object in front of the rangefinder.
// Pass `raw = true` if you want an unfiltered value to be returned
uint64_t HC_SR04::get_range(bool raw = false) {
    if (DEBUG_MODE && HC_SR04_MANUAL) {
        print("HC-SR04 rangefinder debug output, mm: ");
        print(_dist, '\n');
        return _dist;
    }
    else if (to_us_since_boot(get_absolute_time()) - last_time_measured > 100000) {
        delay_us = 0;
        gpio_put(self_trigger, 0);
        sleep_us(5);
        gpio_put(self_trigger, 1);
        sleep_us(10);
        gpio_put(self_trigger, 0);
        echo_start = to_us_since_boot(get_absolute_time());
        last_time_measured = echo_start;
        while (!gpio_get(self_echo)) {
            if ((to_us_since_boot(get_absolute_time()) - echo_start) > 2 * delay_us_max) return MAX_DIST_MM;
        }
        echo_start = to_us_since_boot(get_absolute_time());
        while (gpio_get(self_echo) && delay_us < delay_us_max) {
            delay_us = to_us_since_boot(get_absolute_time()) - echo_start;
        }
        __dist = (delay_us * V_SOUND / 1000) >> 1;
        if (__dist < MAX_DIST_MM - DIST_TOL_MM || to_us_since_boot(get_absolute_time()) - last_time_reliable > RECAPTURE_TIME_US) {
            _dist = __dist;
            last_time_reliable = to_us_since_boot(get_absolute_time());
        }
        if (_dist < MAX_DIST_MM - DIST_TOL_MM) state_active = true;
        else state_active = false;
    }
    filter = (float)HC_SR04_FILTER_COEF / (float)(to_ms_since_boot(get_absolute_time()) - last_time_called);
    if (filter < 1.0) filter = 1.0;
    last_time_called = to_ms_since_boot(get_absolute_time());
    dist += ((float)_dist - dist) / filter;
    if (!state_active || !_state_active) dist = (float)MAX_DIST_MM;
    if (state_active && !_state_active) dist = _dist;
    _state_active = state_active;
    if (raw) return __dist;
    return (uint64_t)dist;
}

// Function that manually sets rangefinder value to given one in manual mode
void HC_SR04::set_range(uint64_t range) {
    if (DEBUG_MODE && HC_SR04_MANUAL) {
        print("Setting HC-SR04 rangefinder value manually to, mm: ");
        print(range, '\n');
        _dist = range;
    }
}
