/**
 * A button driver, originally created for Phystech.Clocks project.
 * 
 * NOTE!!! This diver is designed to be externally configured. Possible additional code:
 * 
 * #include "pico/stdlib.h"
 * #include "hardware/gpio.h"
 * #include ".../io.h"     // A custom console output library
 * #define DEBUG_MODE true // Set USB debug outputs on / off
 * #define BUTTON_PIN 8
 * #define BUTTON_MANUAL false    // Set manual values setting on / off
 * io_init();       // Initialize custom debug output
 * Button button(BUTTON_PIN);  // Get a button instance
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// A class that represents a single button instance
class Button {
    public:
        Button(uint8_t pin);
        void set_state(bool required_state);
        bool get_state();
    private:
        uint8_t self_pin{0};
        bool state{0};
};


// Constructor, initialization
Button::Button(uint8_t pin) {
    self_pin = pin;
    gpio_init(self_pin);
    gpio_set_dir(self_pin, GPIO_IN);
}

// Function that returns the current button state
bool Button::get_state() {
    if (DEBUG_MODE && BUTTON_MANUAL) {
        print("Button debug output: ");
        print(state, '\n');
        return state;
    }
    else {
        return gpio_get(self_pin);
    }
}

// Function that manually sets button state
void Button::set_state(bool required_state) {
    if (DEBUG_MODE && BUTTON_MANUAL) {
        print("Setting button state manually to: ");
        print(required_state, '\n');
        state = required_state;
    }
}
