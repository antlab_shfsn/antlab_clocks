/**
 * An simple PWM driver, originally created for Phystech.Clocks project, using this example:
 * https://github.com/rgrosset/pico-pwm-audio
 * 
 * Note: the speaker driver is not truly object-oriented due to RPi Pico PWM specific and thus you only get one speaker by default.
 * 
 * NOTE!!! This diver is designed to be externally configured. Possible additional code:
 * 
 * #include "pico/stdlib.h"
 * #include "hardware/irq.h"
 * #include "hardware/pwm.h"
 * #include ".../io.h"     // A custom console output library
 * #define DEBUG_MODE true // Set USB debug outputs on / off
 * #define CLOCK_FREQ_KHZ_NOMINAL 176000	// System frequency; 176000 = 44000 * 4; 44100 Hz is a common audio bitrate
 * #define SPEAKER_PIN 28
 * io_init();		// Initialize custom debug output
 * speaker_init();
 * Speaker speaker;		// Get a speaker pseudoinstance
 * set_sys_clock_khz(CLOCK_FREQ_KHZ_NOMINAL, true);
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


#include "sound.h"

static uint32_t wav_position = 0;
static bool state_active = false;
static bool force_off = false;



// A class that represents speaker instances
class Speaker {
	public:
		void play();
		void stop();
};

// Function that turns speaker on
void Speaker::play() {
	if (!state_active) print("Starting playing sound\n");
	state_active = true;
}
// Function that turns speaker off
void Speaker::stop() {
	if (state_active) print("Stopping playing sound\n");
	state_active = false;
}



/*
 * PWM Interrupt Handler which outputs PWM level and advances the
 * current sample.
 * 
 * We repeat the same value for 8 cycles this means sample rate etc
 * adjust by factor of 8   (this is what bitshifting <<3 is doing)
 * Adjust for different bitrate.
 */
void pwm_interrupt_handler() {
	pwm_clear_irq(pwm_gpio_to_slice_num(SPEAKER_PIN));
	if (state_active && !force_off) {
		if (wav_position < (WAV_DATA_LENGTH<<3) - 1) {
			// allow the pwm value to repeat for 8 cycles this is >>3
			pwm_set_gpio_level(SPEAKER_PIN, WAV_DATA[wav_position>>3]);
			wav_position++;
		} else {
			wav_position = 0;
		}
	} else {
		pwm_set_gpio_level(SPEAKER_PIN, 0);
		wav_position = 0;
	}
}


// Hardware initialization
void speaker_init() {
	gpio_set_function(SPEAKER_PIN, GPIO_FUNC_PWM);
	uint audio_pin_slice = pwm_gpio_to_slice_num(SPEAKER_PIN);
	// Setup PWM interrupt to fire when PWM cycle is complete
	pwm_clear_irq(audio_pin_slice);
	pwm_set_irq_enabled(audio_pin_slice, true);
	// set the handle function above
	irq_set_exclusive_handler(PWM_IRQ_WRAP, pwm_interrupt_handler);
	irq_set_enabled(PWM_IRQ_WRAP, true);
	// Setup PWM for audio output
	pwm_config config = pwm_get_default_config();
	/* Base clock 176,000,000 Hz divide by wrap 250 then the clock divider further divides
	* to set the interrupt rate.
	* 
	* 11 KHz is fine for speech. Phone lines generally sample at 8 KHz
	* 
	* So clkdiv should be as follows for given sample rate
	*  8.0f for 11 KHz
	*  4.0f for 22 KHz
	*  2.0f for 44 KHz etc
	*/
	pwm_config_set_clkdiv(&config, 8.0f);
	pwm_config_set_wrap(&config, 250);
	pwm_init(audio_pin_slice, &config, true);
	pwm_set_gpio_level(SPEAKER_PIN, 0);
}

// This should be used ONLY for power saving mode!
void __sound_power__(bool state) {
	force_off = !state;
}
