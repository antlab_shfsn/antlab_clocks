/**
 * An WS2812B addessed LED driver, originally created for Phystech.Clocks project.
 * 
 * Driver supports:
 * - RGB, RBG, GRB, GBR, BRG, BGR color arrangement;
 * - Setting a (0, 0) corner of the matrix
 * - Setting LED addresation config: the main address increase direction and lines connection type
 * 
 * Note: a simple LED strip is a matrix with height or width equals 1, so the driver is not limited to actual matrixes.
 * 
 * NOTE!!! This diver is designed to be externally configured. Possible additional code:
 * 
 * #include "pico/stdlib.h"
 * #include "hardware/spi.h"
 * #include ".../io.h"     // A custom console output library
 * #define DEBUG_MODE true // Set USB debug outputs on / off
 * #define MATRIX_HEIGHT 8
 * #define MATRIX_WIDTH 32
 * #define WS2812B_PIN 3
 * #define WS2812B_GAMMA 2.7
 * #define MAX_WS2812B_DEBUG_OUTPUTS_PER_SEC 5		// Reduce massive debug outputs count
 * io_init();		// Initialize custom debug output
 * spi_init(spi_default, 6250000);	// 0.4 us atomic time for WS2812B means 1/0.4us = 25 MHz switch freq and thus (25/8)*2 = 6.25 MHz SPI baud rate
 * WS2812B screen(WS2812B_PIN, ORDER_RGB, CORNER_LEFT_TOP, DIR_HORIZONTAL, DIR_CONSTANT); // Get a matrix / strip instance
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// WS2812B can have up to 6 different color layouts: RGB, RBG, GRB, GBR, BRG, BGR
enum Color_order { ORDER_RGB, ORDER_RBG, ORDER_GRB, ORDER_GBR, ORDER_BRG, ORDER_BGR };
// Define which corner of the matrix has (0, 0) coordinates
enum Home_corner { CORNER_LEFT_TOP, CORNER_LEFT_BOTTOM, CORNER_RIGHT_TOP, CORNER_RIGHT_BOTTOM };
// Define whether increasing 1-dimentional "strip" coordinate goes horizontal or vertical
enum Main_dir { DIR_HORIZONTAL, DIR_VERTICAL };
// Define whether increasing 1-dimentional "strip" coordinate goes in a constant horiz. / vert. direction or zig-zag
enum Line_conn { DIR_CONSTANT, DIR_ZIGZAG };
// Define matrix should be rotate by 180 deg or not
enum Rotation { ROTATE_0, ROTATE_180 };




// A class that represents a single WS2812B instance, i.e. LED strip or matrix
class WS2812B {
	public:
		WS2812B(uint8_t pin, Color_order order, Home_corner home_corner, Main_dir main_dir, Line_conn zigzag, Rotation rotation);
		void set_image(uint8_t* page);
	private:
		uint8_t self_pin{0};
		uint64_t matrix_size = MATRIX_HEIGHT * MATRIX_WIDTH;
		uint8_t order_r{0};
		uint8_t order_g{0};
		uint8_t order_b{0};
		bool step_dir_x{0};
		bool step_dir_y{0};
		bool dir_horiz{0};
		bool dir_zigzag{0};
		bool rotate_180{0};
		uint8_t page_out[MATRIX_HEIGHT * MATRIX_WIDTH][3]{0};
		uint8_t page_spi[MATRIX_HEIGHT * MATRIX_WIDTH * 3 * 8]{0};
		char page_print[MATRIX_HEIGHT][MATRIX_WIDTH][4]{0};
		uint64_t last_time_called = 0;
		uint64_t min_debug_delay_us = 0;
		uint64_t last_time_sent = 0;
		uint8_t gamma_table[256]{};
	protected:
		void set_order(uint8_t order);
		void set_corner(uint8_t home_corner);
};




// Constructor, initialization
WS2812B::WS2812B(uint8_t pin, Color_order order, Home_corner home_corner, Main_dir main_dir, Line_conn zigzag, Rotation rotation) {
	self_pin = pin;
	set_order(order);
	set_corner(home_corner);
	dir_horiz = main_dir == DIR_HORIZONTAL ? true : false;
	dir_zigzag = zigzag == DIR_ZIGZAG ? true : false;
	rotate_180 = rotation == ROTATE_180 ? true : false;
	gpio_set_function(WS2812B_PIN, GPIO_FUNC_SPI);
	if(DEBUG_MODE) {
		min_debug_delay_us = 1000000 / MAX_WS2812B_DEBUG_OUTPUTS_PER_SEC;
		char* page_print_ptr = (char*)&page_print;
		for (uint32_t ptr = 0; ptr < MATRIX_HEIGHT * MATRIX_WIDTH * 4; ++ptr) *(page_print_ptr + ptr) = ' ';
		for (uint32_t ptr = MATRIX_WIDTH * 4 - 1; ptr < MATRIX_HEIGHT * MATRIX_WIDTH * 4; ptr += MATRIX_WIDTH * 4) *(page_print_ptr + ptr) = '\n';
		page_print[MATRIX_HEIGHT - 1][MATRIX_WIDTH - 1][3] = '\0';
	}
	for (uint16_t i = 0; i < 256; ++i) {
		gamma_table[i] = (uint8_t)(255.0 * pow((float)i / 255.0, WS2812B_GAMMA));
	}
}

// Function that handles internal color order initialization
void WS2812B::set_order(uint8_t order) {
	if (order == ORDER_RBG) { order_r = 0; order_g = 2; order_b = 1; }
	else if (order == ORDER_GRB) { order_r = 1; order_g = 0; order_b = 2; }
	else if (order == ORDER_GBR) { order_r = 2; order_g = 0; order_b = 1; }
	else if (order == ORDER_BRG) { order_r = 1; order_g = 2; order_b = 0; }
	else if (order == ORDER_BGR) { order_r = 2; order_g = 1; order_b = 0; }
	else { order_r = 0; order_g = 1; order_b = 2; }
}

// Function that handles internal (0, 0) corner initialization
void WS2812B::set_corner(uint8_t home_corner) {
	if (home_corner == CORNER_RIGHT_TOP || home_corner == CORNER_RIGHT_BOTTOM) step_dir_x = false;
	else step_dir_x = true;
	if (home_corner == CORNER_LEFT_BOTTOM || home_corner == CORNER_RIGHT_BOTTOM) step_dir_y = false;
	else step_dir_y = true;
}

// Function that updates image displayed on strip / matrix
void WS2812B::set_image(uint8_t* page) {

	if (to_us_since_boot(get_absolute_time()) - last_time_sent > 60) {
		uint64_t pos_x, pos_y, pos_a;
		for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
			for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
				pos_x = step_dir_x ? x : MATRIX_WIDTH - 1 - x;
				pos_y = step_dir_y ? y : MATRIX_HEIGHT - 1 - y;
				pos_x = rotate_180 ? MATRIX_WIDTH - 1 - pos_x : pos_x;
				pos_y = rotate_180 ? MATRIX_HEIGHT - 1 - pos_y : pos_y;
				if (dir_horiz && dir_zigzag && pos_y % 2) pos_x = MATRIX_WIDTH - 1 - pos_x;
				else if (!dir_horiz && dir_zigzag && pos_x % 2) pos_y = MATRIX_HEIGHT - 1 - pos_y;
				pos_a = dir_horiz ? pos_y * MATRIX_WIDTH + pos_x : pos_x * MATRIX_HEIGHT + pos_y;
				page_out[pos_a][order_r] = gamma_table[*(page + y * MATRIX_WIDTH * 3 + x * 3 + 0)];
				page_out[pos_a][order_g] = gamma_table[*(page + y * MATRIX_WIDTH * 3 + x * 3 + 1)];
				page_out[pos_a][order_b] = gamma_table[*(page + y * MATRIX_WIDTH * 3 + x * 3 + 2)];
			}
		}
		for (uint64_t pos = 0; pos < MATRIX_HEIGHT * MATRIX_WIDTH; ++pos) {
			for (uint8_t c = 0; c < 3; ++c) {
				for (uint8_t bit = 0; bit < 8; ++bit) {
					// 0.4 us atomic time for WS2812B means 1/0.4us = 25 MHz switch freq and thus
					// (25/8)*4 = 12.5 MHz SPI baud rate and 0b00111100 : 0b00110000 SPI transmission bit sequencies
					*(page_spi + pos * 24 + c * 8 + bit) = page_out[pos][c] & (0b10000000 >> bit) ? 0b00111100 : 0b00110000;
				}
			}
		}
		spi_write_blocking(spi_default, page_spi, MATRIX_HEIGHT * MATRIX_WIDTH * 3 * 8);
		last_time_sent = to_us_since_boot(get_absolute_time());
	}

	// USB console debug output
	if (DEBUG_MODE && (to_us_since_boot(get_absolute_time()) - last_time_called) > min_debug_delay_us) {
		last_time_called = to_us_since_boot(get_absolute_time());
		const char* palette = "-rR-gG-bB";
		uint16_t max_value = 1;
		uint16_t pc_value = 0;
		for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
			for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
				for (uint8_t c = 0; c < 3; ++c) {
					pc_value = *(page + y * MATRIX_WIDTH * 3 + x * 3 + c);
					max_value = pc_value > max_value ? pc_value : max_value;
				}
			}
		}
		for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
			for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
				for (uint8_t c = 0; c < 3; ++c) {
					page_print[y][x][c] = palette[c * 3 + (uint16_t)*(page + y * MATRIX_WIDTH * 3 + x * 3 + c) * 255 / (max_value * 86)];
				}
			}
		}
		print("\nMatrix debug view:\n\n");
		print((const char*)page_print, '\n');
	}
}
