/**
 * An DS1307 RTC driver, originally created for Phystech.Clocks project.
 * 
 * NOTE!!! This diver is designed to be externally configured. Possible additional code:
 * 
 * #include "pico/stdlib.h"
 * #include "hardware/gpio.h"
 * #include "hardware/i2c.h"
 * #include ".../io.h"     // A custom console output library
 * #define DEBUG_MODE true // Set USB debug outputs on / off
 * #define DS1307_SDA_PIN 20
 * #define DS1307_SCL_PIN 21
 * io_init();       // Initialize custom debug output
 * RTC rtc(DS1307_SDA_PIN, DS1307_SCL_PIN);  // Get an RTC instance
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */



// Custom expanded datetime data structure
struct datetime_s
{
	uint16_t year = 2000;
	uint8_t month = 1;
	uint8_t day = 1;
	uint8_t dotw = 6;		// 1-Mon, 2-Tue, 3-Wed, 4-Thu, 5-Fri, 6-Sat, 0-Sun
	uint8_t hour = 0;
	uint8_t min = 0;
	uint8_t sec = 0;

	bool operator==(datetime_s& other) {
		return hour == other.hour and min == other.min and sec == other.sec;
	}

	bool operator!=(datetime_s& other) {
		return !operator==(other);
	}
};



// A class that represents a single DS1307 RTC instance
class RTC {
	public:
		RTC(uint8_t sda_pin, uint8_t scl_pin);
		bool rtc_fetch_allowed;
		uint16_t get_days_in_year(uint16_t year);
		uint8_t get_days_in_month(uint8_t month, uint16_t days_in_year);
		uint8_t get_day_of_the_week();
		datetime_s get_datetime();
		void set_time(datetime_s time);
		void set_date(datetime_s date);
		void sync_sw_from_hw();
		void sync_hw_from_sw();
	private:
		uint16_t rtc_valid;
		int addr;
		uint8_t registers[7];
		datetime_s datetime;
		uint64_t offset_s{0};
		uint64_t offset_d{0};
	protected:
		void ensure_config(bool read_req);
		int read_raw();
		int write_raw();
		void conv2dt();
		void conv2rg();
		void get_hw_datetime();
		void set_hw_datetime();
};



// Constructor, initialization
RTC::RTC(uint8_t sda_pin, uint8_t scl_pin) {
	rtc_valid = RTC_VALIDATION_COUNT; // Assume that RTC is valid on start
	rtc_fetch_allowed = true;

	addr = 0x68;
	i2c_init(i2c_default, 400 * 1000);
	gpio_set_function(sda_pin, GPIO_FUNC_I2C);
	gpio_set_function(scl_pin, GPIO_FUNC_I2C);
	gpio_pull_up(sda_pin);
	gpio_pull_up(scl_pin);

	// Get initial offsets for 00:00:00, 01.01.2000
	set_time(datetime);
	set_date(datetime);

	// Ensure initial RTC config
	ensure_config(true);
}



// Reads data from ds1307 registers one by one
int RTC::read_raw() {
	int valid;
	for (uint8_t i = 0; i < 7; ++i) {
		valid = i2c_write_timeout_us(i2c_default, addr, &i, 1, true, I2C_TIMEOUT_US);
		if (!valid || valid == PICO_ERROR_GENERIC || valid == PICO_ERROR_TIMEOUT) return 1;
		valid = i2c_read_timeout_us(i2c_default, addr, registers + i, 1, false, I2C_TIMEOUT_US);
		if (!valid || valid == PICO_ERROR_GENERIC || valid == PICO_ERROR_TIMEOUT) return 1;
	}
	return 0;
}



// Writes data to ds1307 registers one by one
int RTC::write_raw() {
	int valid;
	uint8_t buf[2];
	for (uint8_t i = 0; i < 7; ++i) {
		buf[0] = i;
		buf[1] = registers[i];
		valid = i2c_write_timeout_us(i2c_default, addr, buf, 2, true, I2C_TIMEOUT_US);
		if (!valid || valid == PICO_ERROR_GENERIC || valid == PICO_ERROR_TIMEOUT) return 1;
	}
	return 0;
}



// Converts data from ds1307 format to datetime_s format
void RTC::conv2dt() {
	datetime.sec = (registers[0] & 0b1111) + 10 * ((registers[0] >> 4) & 0b111);
	datetime.min = (registers[1] & 0b1111) + 10 * ((registers[1] >> 4) & 0b111);
	datetime.hour = (registers[2] & 0b1111) + 10 * ((registers[2] >> 4) & 0b1) + 20 * ((registers[2] >> 5) & 0b1);
	datetime.day = (registers[4] & 0b1111) + 10 * ((registers[4] >> 4) & 0b11);
	datetime.month = (registers[5] & 0b1111) + 10 * ((registers[5] >> 4) & 0b1);
	datetime.year = 2000 + (registers[6] & 0b1111) + 10 * ((registers[6] >> 4) & 0b1111);
}



// Converts data from datetime_s format to ds1307 format
void RTC::conv2rg() {
	registers[0] = ((((datetime.sec / 10) % 10) << 4) + (datetime.sec % 10)) & 0b01111111;
	registers[1] = (((datetime.min / 10) % 10) << 4) + (datetime.min % 10);
	registers[2] = (((uint8_t)(datetime.hour >= 20) << 5) + ((uint8_t)(datetime.hour >= 10 && datetime.hour < 20) << 4) +
									(datetime.hour % 10)) & 0b00111111;
	registers[4] = (((datetime.day / 10) % 10) << 4) + (datetime.day % 10);
	registers[5] = (((datetime.month / 10) % 10) << 4) + (datetime.month % 10);
	registers[6] = ((((datetime.year % 100) / 10) % 10) << 4) + ((datetime.year % 100) % 10);
}



// Calculates the amount of days in the year depending on whether it is a leap year or not
uint16_t RTC::get_days_in_year(uint16_t year) {
	if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) return 366;
	return 365;
}



// Calculates the amount of days in the month depending on month and whether it is a leap year or not
uint8_t RTC::get_days_in_month(uint8_t month, uint16_t days_in_year) {
	if (month == 1) return 31;
	else if (month == 2 && days_in_year == 365) return 28;
	else if (month == 2 && days_in_year == 366) return 29;
	else if (month == 3) return 31;
	else if (month == 4) return 30;
	else if (month == 5) return 31;
	else if (month == 6) return 30;
	else if (month == 7) return 31;
	else if (month == 8) return 31;
	else if (month == 9) return 30;
	else if (month == 10) return 31;
	else if (month == 11) return 30;
	else if (month == 12) return 31;
	else return 31;
}



// Calculates day of the week for dates from 1 Jan 2000 to 31 Dec 2099
uint8_t RTC::get_day_of_the_week() {
	if (datetime.year < 2000) return 0;
	uint16_t year = 2000;
	uint8_t month = 1;
	uint64_t total_days = 0;
	while (year < datetime.year) total_days += get_days_in_year(year);
	while (month < datetime.month) total_days += get_days_in_month(month, get_days_in_year(year));
	total_days += datetime.day - 1 + 6; // 1 Jan 2000 is saturday
	return total_days % 7;
}



// Ensure RTC config: Clock Halt is 0 and 24h/am-pm is 0
void RTC::ensure_config(bool read_req) {
	int valid = 0;
	if (read_req) valid |= read_raw();
	while (registers[0] & 0b10000000 || registers[2] & 01000000) {
		registers[0] &= 0b01111111;	// Set CH bit to zero to run clocks
		registers[2] &= 0b10111111;	// Set hour mode bit to 0 for 24-hour format
		valid |= write_raw();
		valid |= read_raw();
	}
	// If write timeout is exeeded assume RTC as nonvalid
	if (!valid) rtc_valid = rtc_valid >= RTC_VALIDATION_COUNT ? RTC_VALIDATION_COUNT : rtc_valid + 1;
	else rtc_valid = 0;
}



// Returns datetime_s structure with current RTC date and time
void RTC::get_hw_datetime() {
	// If timeout for read is exeeded assume all gathered data as corrupted
	if (!read_raw()) {
		conv2dt();
		datetime.dotw = get_day_of_the_week();
	} else {
		conv2rg(); // Prevent further use of corrupted data
		rtc_valid = 0;
	}
}



// Sets self datetime to DS1307
void RTC::set_hw_datetime() {
	conv2rg();
	// If write timeout is exeeded assume RTC as nonvalid
	if (!write_raw()) rtc_valid = rtc_valid >= RTC_VALIDATION_COUNT ? RTC_VALIDATION_COUNT : rtc_valid + 1;
	else rtc_valid = 0;
}



// Returns datetime_s structure with current RTC date and time
datetime_s RTC::get_datetime() {
	if (rtc_valid >= RTC_VALIDATION_COUNT && rtc_fetch_allowed) {
		sync_sw_from_hw();
	} else {
		// If RTC is assumed to be broken try to write to it attempting to fix.
		sync_hw_from_sw();
	}
	uint64_t absolute_s = to_us_since_boot(get_absolute_time()) / 1000000 + offset_s + offset_d;
	uint8_t sec = absolute_s % 60;
	absolute_s /= 60;
	uint8_t min = absolute_s % 60;
	absolute_s /= 60;
	uint8_t hour = absolute_s % 24;
	absolute_s /= 24;
	uint8_t dotw = (6 + absolute_s) % 7;
	uint16_t year = 2000;
	uint16_t days_in_year = get_days_in_year(year);
	while (absolute_s >= days_in_year) {
		++year;
		absolute_s -= days_in_year;
		days_in_year = get_days_in_year(year);
	}
	uint8_t month = 1;
	uint8_t days_in_month = get_days_in_month(month, days_in_year);
	while (absolute_s >= days_in_month) {
		++month;
		absolute_s -= days_in_month;
		days_in_month = get_days_in_month(month, days_in_year);
	}
	uint8_t day = absolute_s + 1;
	datetime = {
		.year = year,
		.month = month,
		.day = day,
		.dotw = dotw,
		.hour = hour,
		.min = min,
		.sec = sec
	};
	return datetime;
}



// Sets given time in datetime_s structure to RTC
void RTC::set_time(datetime_s time) {
	offset_s = 0 - (to_us_since_boot(get_absolute_time()) / 1000000);
	offset_s += time.hour * 3600;
	offset_s += time.min * 60;
	offset_s += time.sec;
}



// Sets given date in datetime_s structure to RTC
void RTC::set_date(datetime_s date) {
	offset_d = 0;
	uint16_t year = date.year;
	while (year > 2000) {
		offset_d += get_days_in_year(year) * 86400;
		--year;
	}
	uint8_t month = date.month;
	while(month > 1) {
		offset_d += get_days_in_month(month, get_days_in_year(year)) * 86400;
		--month;
	}
	offset_d += (date.day - 1) * 86400;
}



// Pulls datetime from DS1307 hardware RTC to software RTC
void RTC::sync_sw_from_hw() {
	get_hw_datetime();
	set_time(datetime);
	set_date(datetime);
	ensure_config(false);
}



// Pushes datetime from software RTC to DS1307 hardware RTC
void RTC::sync_hw_from_sw() {
	set_hw_datetime();
	ensure_config(true);
}
