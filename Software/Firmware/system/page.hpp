/**
 * Page class for Phystech.Clocks project
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// A class that represents a single page instance
class Page {
	public:
		Page();
		void add(uint8_t color_array[3]);
		void add(Page page);
		void mul(uint8_t color_array[3]);
		void mul(Page page);
		void set(Page page);
		void set(uint8_t* page);
		uint8_t* get_pointer();
		void set_pixel(uint64_t x, uint64_t y, uint8_t color_array[3]);
	private:
		uint8_t page_array[MATRIX_HEIGHT][MATRIX_WIDTH][3]{0};
};


// Constructor, initialization
Page::Page() {
	for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
			for (uint8_t c = 0; c < 3; ++c) page_array[y][x][c] = 0;
		}
	}
}

// Adds given color vector or another page (per-pixel)
void Page::add(uint8_t color_array[3]) {
	uint16_t self_pixel, foreign_pixel;
	for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
			for (uint8_t c = 0; c < 3; ++c) {
				self_pixel = page_array[y][x][c];
				foreign_pixel = color_array[c];
				page_array[y][x][c] = (self_pixel + foreign_pixel > 255) ? 255 : (uint8_t)(self_pixel + foreign_pixel);
			}
		}
	}
}

// Adds given color vector or another page (per-pixel)
void Page::add(Page page) {
	uint16_t self_pixel, foreign_pixel;
	for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
			for (uint8_t c = 0; c < 3; ++c) {
				self_pixel = page_array[y][x][c];
				foreign_pixel = page.page_array[y][x][c];
				page_array[y][x][c] = (self_pixel + foreign_pixel > 255) ? 255 : (uint8_t)(self_pixel + foreign_pixel);
			}
		}
	}
}

// Multiplies by given color vector or another page (per-pixel)
void Page::mul(uint8_t color_array[3]) {
	uint16_t self_pixel, foreign_pixel;
	for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
			for (uint8_t c = 0; c < 3; ++c) {
				self_pixel = page_array[y][x][c];
				foreign_pixel = color_array[c];
				page_array[y][x][c] = (uint8_t)((self_pixel * foreign_pixel / 255));
			}
		}
	}
}

// Multiplies by given color vector or another page (per-pixel)
void Page::mul(Page page) {
	uint16_t self_pixel, foreign_pixel;
	for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
			for (uint8_t c = 0; c < 3; ++c) {
				self_pixel = page_array[y][x][c];
				foreign_pixel = page.page_array[y][x][c];
				page_array[y][x][c] = (uint8_t)((self_pixel * foreign_pixel / 255));
			}
		}
	}
}

// Sets page from another given page or a raw array given as pointer
void Page::set(Page page) {
	for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
			for (uint8_t c = 0; c < 3; ++c) {
				page_array[y][x][c] = page.page_array[y][x][c];
			}
		}
	}
}

// Sets page from another given page or a raw array given as pointer
void Page::set(uint8_t* page) {
	for (uint64_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (uint64_t x = 0; x < MATRIX_WIDTH; ++x) {
			for (uint8_t c = 0; c < 3; ++c) {
				page_array[y][x][c] = *(page + y * MATRIX_WIDTH * 3 + x * 3 + c);
			}
		}
	}
}

// Returns a raw page pointer for external classes
uint8_t* Page::get_pointer() {
	return (uint8_t*)&page_array;
}

// Indivadual pixel setting
void Page::set_pixel(uint64_t x, uint64_t y, uint8_t color_array[3]) {
	page_array[y][x][0] = color_array[0];
	page_array[y][x][1] = color_array[1];
	page_array[y][x][2] = color_array[2];
}
