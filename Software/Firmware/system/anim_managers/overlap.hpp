/**
 * Simple overlap animation manager for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// A class that represents a simple overlap animation manager instance
class Overlap_anim_manager {
	public:
		void get_anim(uint8_t* appliance_1, uint8_t* appliance_2, uint8_t* anim_page);
};


// Gets pointers to appliance_1, appliance_2, anim_page, animation start timestamp and animation lenght (duration) in us.
// Forms animation data on given pointers.
void Overlap_anim_manager::get_anim(uint8_t* appliance_1, uint8_t* appliance_2, uint8_t* anim_image) {
	for (uint64_t i = 0; i < MATRIX_HEIGHT * MATRIX_WIDTH * 3; ++i) {
		*(appliance_1 + i) = 255;
		*(appliance_2 + i) = 255;
		*(anim_image + i) = 0;
	}
}
