/**
 * Double fade animation manager for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// A class that represents a double fade animation manager instance
class Double_fade_anim_manager {
	public:
		void get_anim(uint8_t* appliance_1, uint8_t* appliance_2, uint8_t* anim_page, uint64_t start_time, uint64_t duration);
};


// Gets pointers to appliance_1, appliance_2, anim_page, animation start timestamp and animation lenght (duration) in us.
// Forms animation data on given pointers.
void Double_fade_anim_manager::get_anim(uint8_t* appliance_1, uint8_t* appliance_2, uint8_t* anim_image, uint64_t start_time, uint64_t duration) {
	uint64_t progress = duration ? (to_us_since_boot(get_absolute_time()) - start_time) * 255 / duration : 255;
	// Unreversed filter
	progress = (255 - progress);
	// Pseudoexponential filter of degree 2
	progress = progress * progress / 255;
	// Appliances and image forming
	for (uint64_t i = 0; i < MATRIX_HEIGHT * MATRIX_WIDTH * 3; ++i) {
		*(appliance_1 + i) = 255 - progress;
		*(appliance_2 + i) = progress;
		*(anim_image + i) = 0;
	}
}
