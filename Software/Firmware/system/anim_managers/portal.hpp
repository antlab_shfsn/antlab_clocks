/**
 * Portal animation manager for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


#define PORTAL_WIDTH 2


// A class that represents a portal animation manager instance
class Portal_anim_manager {
	public:
		void get_anim(uint8_t* appliance_1, uint8_t* appliance_2, uint8_t* anim_page, uint64_t start_time, uint64_t duration, int64_t x0, int64_t y0);
};


// Gets pointers to appliance_1, appliance_2, anim_page, animation start timestamp, animation lenght (duration) in us and center position.
// Forms animation data on given pointers.
void Portal_anim_manager::get_anim(uint8_t* appliance_1, uint8_t* appliance_2, uint8_t* anim_image, uint64_t start_time, uint64_t duration, int64_t x0, int64_t y0) {
	int64_t progress = (to_us_since_boot(get_absolute_time()) - start_time) * 3200 / duration;
	progress = progress * progress / 3200 * progress / 3200;
	// Appliances and image forming
	int64_t delta_x, delta_y, dist2, d_dist2, pixel;
	for (int64_t y = 0; y < MATRIX_HEIGHT; ++y) {
		for (int64_t x = 0; x < MATRIX_WIDTH; ++x) {
			delta_x = x - x0;
			delta_y = y - y0;
			// dist2 is a circle radius squared
			dist2 = delta_x * delta_x + delta_y * delta_y;
			d_dist2 = abs(delta_x) + abs(delta_y);
			for (uint8_t c = 0; c < 3; ++c) {
				// Old page inside the circle, new one out of the circle
				*(appliance_1 + y * MATRIX_WIDTH * 3 + x * 3 + c) = dist2 < progress ? 255 : 0;
				*(appliance_2 + y * MATRIX_WIDTH * 3 + x * 3 + c) = dist2 < progress ? 0 : 255;
				// Draw a light blue circle
				pixel = c == 2 ? 255 : 192;
				pixel = pixel - (dist2 < progress ? (progress - dist2) : (dist2 - progress)) * pixel / (PORTAL_WIDTH * d_dist2);
				pixel = pixel * 255 / 192;
				pixel = pixel > 255 ? 255 : pixel;
				*(anim_image + y * MATRIX_WIDTH * 3 + x * 3 + c) = (uint8_t)(pixel < 0 ? 0 : pixel);
			}
		}
	}
}
