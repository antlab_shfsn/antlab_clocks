/**
 * A custom debug output library via USB.
 * 
 * Note that all the functions here will be automatically disabled if `DEBUG_MODE` is set to `false`.
 * 
 * NOTE!!! This diver is designed to be externally configured. Possible additional code:
 * 
 * #include "pico/stdlib.h"
 * #define DEBUG_MODE true // Set USB debug outputs on / off
 * io_init();       // Initialize custom debug output
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// Function initializes USB IO. Returns 0 if successful.
int io_init() {
	if (DEBUG_MODE) {
		stdio_init_all();
		return 0;
	} else return 1;
}

// Simple `printf` wrap function wrap that includes debug IO. Takes 1 arg strictly. Returns 0 if successful.
int print(const char* arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%s%c", arg, end); return 0; } else return 1;
}
int print(char arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%c%c", arg, end); return 0; } else return 1;
}
int print(bool arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%hhu%c", arg, end); return 0; } else return 1;
}
int print(int8_t arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%hhd%c", arg, end); return 0; } else return 1;
}
int print(int16_t arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%hd%c", arg, end); return 0; } else return 1;
}
int print(int32_t arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%d%c", arg, end); return 0; } else return 1;
}
int print(int64_t arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%lld%c", arg, end); return 0; } else return 1;
}
int print(uint8_t arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%hhu%c", arg, end); return 0; } else return 1;
}
int print(uint16_t arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%hu%c", arg, end); return 0; } else return 1;
}
int print(uint32_t arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%u%c", arg, end); return 0; } else return 1;
}
int print(uint64_t arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%llu%c", arg, end); return 0; } else return 1;
}
int print(float arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%f%c", arg, end); return 0; } else return 1;
}
int print(double arg, char end = '\0') {
	if (DEBUG_MODE) { printf("%lf%c", arg, end); return 0; } else return 1;
}
