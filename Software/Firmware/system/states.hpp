/**
 * Hardwawre, software and pages states for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


struct Button_logic {
	bool press = false, _press = false;
	uint64_t press_start_us = 0;
	uint64_t len_us = 0, _len_us = 0;
	bool event = false;
};


struct HW_states {
	uint64_t timestamp_us = 0;
	datetime_s datetime;

	uint64_t range = MAX_DIST_MM, _range = MAX_DIST_MM;
	uint64_t range_interaction_event_us = 0;
	uint64_t range_interaction_len_us = 0;
	bool range_active = false;

	Button_logic btn_1, btn_2;

	uint8_t brightness = 127;
	uint8_t brightness_1 = brightness;
	uint8_t brightness_2 = brightness;
	uint8_t brt_array[3];
};


struct SW_states {
	bool was_rebooted = true;

	uint64_t _timestamp_us = 0;

	Mode_names current_mode;
	Mode_names _current_mode;

	uint8_t range_state_machine = 0;
	Button_logic btn_up, btn_dn;

	uint64_t last_interaction_us = 0;
	bool home_return = false;

	bool speaker = false;
	bool _speaker = false;
	uint64_t speaker_start_timestamp = 0;
};


struct Indicator_state {
	uint8_t color[3] = {73, 104, 40};	// Lime
	uint8_t highlight_color[3] = {217, 255, 177}; // Light lime
};


struct Clocks_state {
	datetime_s timestamp = {.hour = 0, .min = 0, .sec = 0};
	uint8_t color[3] = {255, 216, 106};	// Light yellow
	uint8_t highlight_color[3] = {0, 0, 255}; // Blue
	Clocks_setting setting = NONE_CLOCK_SET;
	Clocks_setting _setting = NONE_CLOCK_SET;
};


struct Timer_state {
	datetime_s timestamp = {.hour = 0, .min = 5, .sec = 0};
	datetime_s duration = timestamp;
	uint64_t _duration = duration.hour * 3600000000 + duration.min * 60000000 + duration.sec * 1000000;
	int64_t time_left = (int64_t)_duration;
	int64_t _time_left = (int64_t)_duration;
	uint64_t time_start = 0;
	bool running = false;
	uint8_t color[3];
	uint8_t color_stop[3] = {44, 255, 207};	// Pale green
	uint8_t color_start[3] = {37, 211, 102};	// Green
	uint8_t color_middle[3] = {255, 242, 28};	// Yellow
	uint8_t color_end[3] = {233, 27, 93};	// Red
	uint8_t highlight_color[3] = {0, 0, 255}; // Blue
	Clocks_setting setting = NONE_CLOCK_SET;
	bool page_call = false;
};


struct Stopwatch_state {
	datetime_s timestamp = {.hour = 0, .min = 0, .sec = 0};
	uint64_t time_start = 0;
	bool running = false;
	uint8_t color[3];
	uint8_t color_run[3] = {37, 176, 243};	// Blue
	uint8_t color_stop[3] = {0, 122, 187};	// Dark blue
	uint8_t highlight_color[3] = {0, 0, 255}; // Blue
};


struct Alarm_state {
	datetime_s timestamp = {.hour = 7, .min = 0, .sec = 0};
	bool set = false;
	bool trigger = false;
	bool _trigger = false;
	uint8_t color[3];
	uint8_t color_set[3] = {255, 165, 42};	// Orange
	uint8_t color_unset[3] = {182, 108, 29};	// Dark orange
	uint8_t highlight_color[3] = {0, 0, 255}; // Blue
	Clocks_setting setting = NONE_CLOCK_SET;
	uint8_t state_machine = 0;
	bool page_call = false;
};
