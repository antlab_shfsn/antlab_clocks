/**
 * Modes logic for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// Clocks mode interal logic
void logic_clocks(HW_states* hw, SW_states* sw, Clocks_state* clock) {
	if (sw->current_mode == CLOCKS) {
		// Setting select
		if (hw->btn_1.event) clock->setting = (Clocks_setting)(((uint8_t)clock->setting + 1) % 4);

		// Timestamp management, increase button
		switch (clock->setting) {
			case NONE_CLOCK_SET:
				clock->timestamp = hw->datetime;
				break;

			case HOUR_SET:
				if (hw->btn_2.event) clock->timestamp.hour = (clock->timestamp.hour + 1) % 24;
				break;

			case MINUTE_SET:
				if (hw->btn_2.event) clock->timestamp.min = (clock->timestamp.min + 1) % 60;
				break;

			case SECOND_SET:
				if (hw->btn_2.event) clock->timestamp.sec = (clock->timestamp.sec + 1) % 60;
				break;
			
			default:
				break;
		}
	} else {
		// Inactive mode actions
		clock->timestamp = hw->datetime;
	}
}


// Timer mode interal logic
void logic_timer(HW_states* hw, SW_states* sw, Timer_state* timer) {
	if (timer->running) {
		timer->time_left = (int64_t)timer->_duration - (int64_t)(hw->timestamp_us - timer->time_start);
		timer->time_left = timer->time_left < 0 ? 0 : timer->time_left;

		// Force call mode on runout
		if (!timer->time_left && timer->_time_left) {
			sw->speaker = true;
			timer->page_call = true;
		}
		else timer->page_call = false;

		// Run/stop button
		if (hw->btn_2.event && sw->current_mode == TIMER) timer->running = false;

		// User reset on the end of the run
		if (timer->time_left == 0 && (hw->btn_1.event || hw->btn_2.event || hw->range_active)) {
			timer->running = false;
			timer->time_left = (int64_t)timer->_duration;
		}
	} else {
		if (sw->current_mode == TIMER) {
			// Setting select
			if (hw->btn_1.event && timer->time_left == (int64_t)timer->_duration) timer->setting = (Clocks_setting)(((uint8_t)timer->setting + 1) % 4);

			// Duration management
			switch (timer->setting) {
				case HOUR_SET:
					if (hw->btn_2.event) timer->duration.hour = (timer->duration.hour + 1) % 100;
					break;

				case MINUTE_SET:
					if (hw->btn_2.event) timer->duration.min = (timer->duration.min + 1) % 60;
					break;

				case SECOND_SET:
					if (hw->btn_2.event) timer->duration.sec = (timer->duration.sec + 1) % 60;
					break;
				
				default:
					break;
			}
			if (timer->setting != NONE_CLOCK_SET) {
				timer->_duration =
					(uint64_t)timer->duration.hour * 3600000000 + (uint64_t)timer->duration.min * 60000000 + (uint64_t)timer->duration.sec * 1000000;
				timer->time_left = (int64_t)timer->_duration;
			} else {
				// Prevent zero time to be set
				if (!timer->_duration) {
					timer->duration.sec = 1;
					timer->_duration = 1000000;
					timer->time_left = (int64_t)timer->_duration;
				}

				// Reset button
				if (hw->btn_1.event && timer->time_left != (int64_t)timer->_duration) timer->time_left = (int64_t)timer->_duration;
				// Run/stop button
				if (hw->btn_2.event) timer->running = true;
			}
		}
		
		timer->time_start = hw->timestamp_us - (timer->_duration - timer->time_left);
	}

	// Save prev time left
	timer->_time_left = timer->time_left;

	// Prepare output
	timer->timestamp = {
		.hour = (uint8_t)(timer->time_left / 3600000000),
		.min = (uint8_t)((timer->time_left / 60000000) % 60),
		.sec = (uint8_t)((timer->time_left / 1000000) % 60)
	};
	// Color mixing
	// green -> yellow -> red
	int32_t timer_pos = (uint8_t)((255 * (timer->_duration - (uint64_t)timer->time_left)) / timer->_duration);
	int32_t color_mix[3];
	int32_t color_on_timer;
	color_mix[0] = 255 - 2 * timer_pos;
	color_mix[1] = 255 - abs(2 * (timer_pos - 128));
	color_mix[2] = 2 * (timer_pos - 128);
	if (color_mix[0] < 0) color_mix[0] = 0;
	if (color_mix[1] < 0) color_mix[1] = 0;
	if (color_mix[2] < 0) color_mix[2] = 0;
	for (uint8_t c = 0; c < 3; ++c) {
		color_on_timer =
			((int32_t)timer->color_start[c] * color_mix[0] +
			(int32_t)timer->color_middle[c] * color_mix[1] +
			(int32_t)timer->color_end[c] * color_mix[2]) / 255;
		if (color_on_timer > 255) color_on_timer = 255;
		timer->color[c] = timer->running ? (uint8_t)color_on_timer : timer->color_stop[c];
	}
}


// Stopwatch mode interal logic
void logic_stopwatch(HW_states* hw, SW_states* sw, Stopwatch_state* stopwatch) {
	uint64_t time_diff;
	// If running update timestamp,
	// if not running update start time
	if (stopwatch->running) {
		time_diff = hw->timestamp_us - stopwatch->time_start;
		time_diff = time_diff > 359999000000 ? 359999000000 : time_diff; // 99h 59m 59s max
		stopwatch->timestamp = {
			.hour = (uint8_t)(time_diff / 3600000000),
			.min = (uint8_t)((time_diff / 60000000) % 60),
			.sec = (uint8_t)((time_diff / 1000000) % 60)
		};

		// Run/stop button
		if (hw->btn_2.event && sw->current_mode == STOPWATCH) stopwatch->running = false;
	} else {
		time_diff =
			(uint64_t)stopwatch->timestamp.hour * 3600000000 + (uint64_t)stopwatch->timestamp.min * 60000000 + (uint64_t)stopwatch->timestamp.sec * 1000000;
		stopwatch->time_start = hw->timestamp_us - time_diff;

		// Reset button
		if (hw->btn_1.event && sw->current_mode == STOPWATCH) stopwatch->timestamp = { .hour = 0, .min = 0, .sec = 0 };
		// Run/stop button
		if (hw->btn_2.event && sw->current_mode == STOPWATCH) stopwatch->running = true;
	}
	
	// Define color
	for (uint8_t c = 0; c < 3; ++c) {
		stopwatch->color[c] = stopwatch->running ? stopwatch->color_run[c] : stopwatch->color_stop[c];
	}
}


// Alarm mode interal logic
void logic_alarm(HW_states* hw, SW_states* sw, Alarm_state* alarm) {
	if (sw->current_mode == ALARM) {
		// Set/unset button
		if (hw->btn_2.event && alarm->setting == NONE_CLOCK_SET) alarm->set = !alarm->set;

		// Setting select
		if (hw->btn_1.event) alarm->setting = (Clocks_setting)(((uint8_t)alarm->setting + 1) % 3);

		// Timestamp management, increase button
		switch (alarm->setting) {
			case HOUR_SET:
				if (hw->btn_2.event) alarm->timestamp.hour = (alarm->timestamp.hour + 1) % 24;
				break;

			case MINUTE_SET:
				if (hw->btn_2.event) alarm->timestamp.min = (alarm->timestamp.min + 1) % 60;
				break;
			
			default:
				break;
		}
	}

	// React on pre-set time
	if (alarm->set && alarm->timestamp == hw->datetime) alarm->trigger = true;
	else alarm->trigger = false;
	if (!alarm->_trigger && alarm->trigger) {
		sw->speaker = true;
		alarm->page_call = true;
	}
	else alarm->page_call = false;
	alarm->_trigger = alarm->trigger;
	
	// Prepare output color
	for (uint8_t c = 0; c < 3; ++c) {
		alarm->color[c] = alarm->set ? alarm->color_set[c] : alarm->color_unset[c];
	}
}
