/**
 * Modes change system or "queue" for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


#define QUEUE_LEN 16
#define MODES_COUNT 4
enum Mode_names { ALARM, STOPWATCH, TIMER, CLOCKS, NONE_MODE };	// NONE_MODE must be thae last one
Mode_names queue_modes[QUEUE_LEN];
enum Anim_types { DFADE_CLOCKS, PORTAL_UD, PORTAL_DU, NONE_ANIM };	// NONE_ANIM must be the last one
Anim_types queue_anims[QUEUE_LEN];
uint64_t queue_timestamps[QUEUE_LEN];



// Add a page to the staging queue
void add_to_queue(Mode_names mode, Anim_types anim, uint64_t timestamp_us) {
	for (uint8_t i = 1; i < QUEUE_LEN; ++i) {
		if (queue_modes[i] == NONE_MODE) {
			queue_modes[i] = mode;
			queue_anims[i] = anim;
			queue_timestamps[i] = timestamp_us;
			break;
		}
	}
}


// Refreshes staging queue and removes old pages
void refresh_queue(uint64_t timestamp_us) {
	uint8_t pos = 1;
	uint64_t endurance;
	uint64_t max_endurance;
	while (pos < QUEUE_LEN - 1 && queue_modes[pos] != NONE_MODE) {
		endurance = timestamp_us - queue_timestamps[pos];
		switch (queue_anims[pos]) {
		case DFADE_CLOCKS:
			max_endurance = CLOCKS_ANIM_DURATION_US;
			break;
		case PORTAL_DU:
			max_endurance = MODE_CHANGE_ANIM_DURATION_US;
			break;
		case PORTAL_UD:
			max_endurance = MODE_CHANGE_ANIM_DURATION_US;
			break;
		
		default:
			break;
		}
		if (endurance > max_endurance) {
			for (uint8_t i = pos - 1; i < QUEUE_LEN - 1; ++i) {
				queue_modes[i] = queue_modes[i + 1];
				queue_anims[i] = queue_anims[i + 1];
				queue_timestamps[i] = queue_timestamps[i + 1];
			}
			queue_anims[0] = NONE_ANIM;
			queue_timestamps[0] = 0;
			queue_modes[QUEUE_LEN - 1] = NONE_MODE;
			queue_anims[QUEUE_LEN - 1] = NONE_ANIM;
			queue_timestamps[QUEUE_LEN - 1] = 0;
		} else ++pos;
	}
}
