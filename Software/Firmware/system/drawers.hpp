/**
 * Page draw call functions for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// Draws entire indicator page, including animation into page_1
void draw_page_indicator() {
	indicator_page_manager.get_page(	page_1.get_pointer(),
																		hw_states.range,
																		hw_states.range_interaction_len_us,
																		RANGE_ACTIVE_TIMEOUT_US,
																		RANGE_PRESS_ZONE_MM,
																		indicator_state.color,
																		indicator_state.highlight_color );
}


// Draws entire clocks page, including animation into page_1, uses page_1 and page_2
void draw_page_clocks() {
	clocks_page_manager.get_page(	page_1.get_pointer(),
																clock_state.timestamp,
																clock_state.setting,
																clock_state.color,
																clock_state.highlight_color );
	// Time change animation
	if (to_us_since_boot(get_absolute_time()) - clocks_page_manager.last_time_changed < CLOCKS_ANIM_DURATION_US) {
		clocks_page_manager.get_page( page_2.get_pointer(),
																	clock_state.timestamp,
																	clock_state.setting,
																	clock_state.color,
																	clock_state.highlight_color,
																	true );
		dfade_manager.get_anim(	appliance_1.get_pointer(),
														appliance_2.get_pointer(),
														anim_image.get_pointer(),
														clocks_page_manager.last_time_changed,
														clock_state.setting == NONE_CLOCK_SET ? CLOCKS_ANIM_DURATION_US : 0 );
		compositor.compose(	page_1.get_pointer(),
												page_2.get_pointer(),
												appliance_1.get_pointer(),
												appliance_2.get_pointer(),
												anim_image.get_pointer());
	}
}


// Draws entire timer page, including animation into page_1, uses page_1 and page_2
void draw_page_timer() {
	timer_page_manager.get_page(	page_1.get_pointer(),
																timer_state.timestamp,
																timer_state.setting,
																timer_state.color,
																timer_state.highlight_color );
	// Time change animation
	if (to_us_since_boot(get_absolute_time()) - timer_page_manager.last_time_changed < CLOCKS_ANIM_DURATION_US) {
		timer_page_manager.get_page( page_2.get_pointer(),
																	timer_state.timestamp,
																	timer_state.setting,
																	timer_state.color,
																	timer_state.highlight_color,
																	true );
		dfade_manager.get_anim(	appliance_1.get_pointer(),
														appliance_2.get_pointer(),
														anim_image.get_pointer(),
														timer_page_manager.last_time_changed,
														timer_state.setting == NONE_CLOCK_SET ? CLOCKS_ANIM_DURATION_US : 0 );
		compositor.compose(	page_1.get_pointer(),
												page_2.get_pointer(),
												appliance_1.get_pointer(),
												appliance_2.get_pointer(),
												anim_image.get_pointer());
	}
}


// Draws entire stopwatch page, including animation into page_1, uses page_1 and page_2
void draw_page_stopwatch() {
	stopwatch_page_manager.get_page(	page_1.get_pointer(),
																stopwatch_state.timestamp,
																NONE_CLOCK_SET,
																stopwatch_state.color,
																stopwatch_state.highlight_color );
	// Time change animation
	if (to_us_since_boot(get_absolute_time()) - stopwatch_page_manager.last_time_changed < CLOCKS_ANIM_DURATION_US) {
		stopwatch_page_manager.get_page( page_2.get_pointer(),
																	stopwatch_state.timestamp,
																	NONE_CLOCK_SET,
																	stopwatch_state.color,
																	stopwatch_state.highlight_color,
																	true );
		dfade_manager.get_anim(	appliance_1.get_pointer(),
														appliance_2.get_pointer(),
														anim_image.get_pointer(),
														stopwatch_page_manager.last_time_changed,
														CLOCKS_ANIM_DURATION_US );
		compositor.compose(	page_1.get_pointer(),
												page_2.get_pointer(),
												appliance_1.get_pointer(),
												appliance_2.get_pointer(),
												anim_image.get_pointer());
	}
}


// Draws entire clocks page, including animation into page_1, uses page_1 and page_2
void draw_page_alarm() {
	alarm_page_manager.get_page(	page_1.get_pointer(),
																alarm_state.timestamp,
																alarm_state.setting,
																alarm_state.color,
																alarm_state.highlight_color,
																false, true );
	// Time change animation
	if (to_us_since_boot(get_absolute_time()) - alarm_page_manager.last_time_changed < CLOCKS_ANIM_DURATION_US) {
		alarm_page_manager.get_page( page_2.get_pointer(),
																	alarm_state.timestamp,
																	alarm_state.setting,
																	alarm_state.color,
																	alarm_state.highlight_color,
																	true, true );
		dfade_manager.get_anim(	appliance_1.get_pointer(),
														appliance_2.get_pointer(),
														anim_image.get_pointer(),
														alarm_page_manager.last_time_changed,
														alarm_state.setting == NONE_CLOCK_SET ? CLOCKS_ANIM_DURATION_US : 0 );
		compositor.compose(	page_1.get_pointer(),
												page_2.get_pointer(),
												appliance_1.get_pointer(),
												appliance_2.get_pointer(),
												anim_image.get_pointer());
	}
}
