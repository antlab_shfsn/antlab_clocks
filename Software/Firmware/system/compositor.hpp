/**
 * Image compositor for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


// A class that represents a Compositor instance
class Compositor {
	public:
		void compose(uint8_t* page_res, uint8_t* page_1, uint8_t* appliance_1, uint8_t* appliance_2, uint8_t* anim_image);
};


// Takes pointers to page_1 (page_res), page_2, appliance_1, appliance_2 and anim_image.
// Outputs resulting image to poge_1 (page_res).
void Compositor::compose(uint8_t* page_res, uint8_t* page_2, uint8_t* appliance_1, uint8_t* appliance_2, uint8_t* anim_image) {
	for (uint64_t i = 0; i < MATRIX_HEIGHT * MATRIX_WIDTH * 3; ++i) {
		// Appliance
		uint64_t pixel = (uint64_t)*(page_res + i) * (uint64_t)*(appliance_1 + i) + (uint64_t)*(page_2 + i) * (uint64_t)*(appliance_2 + i);
		// Animation image
		pixel = pixel / 255 + (uint64_t)*(anim_image + i);
		// Limit brightness
		if (pixel > 255) pixel = 255;
		*(page_res + i) = (uint8_t)pixel;
	}
}
