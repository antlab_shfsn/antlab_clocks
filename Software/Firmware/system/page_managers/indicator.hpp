/**
 * Indicator page manager for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


#define POINT_WIDTH 2
#define INDICATOR_FILTER_COEF 100

// A class that represents an indicator page manager instance
class Indicator_page_manager {
	public:
		Indicator_page_manager();
		void get_page(uint8_t* page, uint64_t range, uint64_t int_time, uint64_t timeout, uint64_t press_zone, uint8_t color[3], uint8_t highlight_color[3]);
		uint64_t last_time_changed;
	protected:
		uint64_t last_time_int = 0x7000000000000000;
		uint64_t last_time_called = 0;
		uint16_t brt;
		int16_t point_brt;
		int16_t pos;
		int16_t indicator[9][3];
		int16_t _indicator[9][3];
		float filter = 0;
};


// Constructor, initialization
Indicator_page_manager::Indicator_page_manager() {
	for (uint8_t i = 0; i < 9; ++i) {
		for (uint8_t c = 0; c < 3; ++c) {
			indicator[i][c] = 0;
			_indicator[i][c] = 0;
		}
	}
}

// Gets pointer to page, current range, interaction time, timeout, press zone and color vectors
// Outputs relulting page to given pointer
void Indicator_page_manager::get_page(uint8_t* page, uint64_t range, uint64_t int_time, uint64_t timeout, uint64_t press_zone, uint8_t color[3], uint8_t highlight_color[3]) {
	if (int_time) last_time_int = to_us_since_boot(get_absolute_time());

	// Calculate brightness
	if (int_time && int_time < timeout) brt = (uint16_t)(255 * int_time / timeout);
	else if (!int_time && to_us_since_boot(get_absolute_time()) - last_time_int < timeout) 
		brt = (uint16_t)(255 * (timeout + last_time_int - to_us_since_boot(get_absolute_time())) / timeout);
	else if (!int_time) brt = 0;
	else brt = 255;
	//brt = brt * brt / 255;

	// Prepare filter
	filter = (float)INDICATOR_FILTER_COEF / (float)(to_ms_since_boot(get_absolute_time()) - last_time_called);
	if (filter < 1.0) filter = 1.0;
	last_time_called = to_ms_since_boot(get_absolute_time());

	// Form an indicator
	pos = (range - MIN_DIST_MM - press_zone) * 255 / (MAX_DIST_MM - MIN_DIST_MM - 2 * press_zone);
	for (int8_t i = 0; i < 9; ++i) {
		for (uint8_t c = 0; c < 3; ++c) {
			if (range == 0 || range == MAX_DIST_MM || brt < 255) indicator[i][c] = color[c];
			else if (range > MAX_DIST_MM - press_zone || range < MIN_DIST_MM + press_zone) indicator[i][c] = highlight_color[c];
			else {
				point_brt = POINT_WIDTH * 255 / 9 - abs(pos - i * 255 / 9);
				if (point_brt < 0) point_brt = 0;
				point_brt = point_brt * 9 / POINT_WIDTH;
				indicator[i][c] = point_brt * highlight_color[c] / 255 + (255 - point_brt) * color[c] / 255;
			}
			_indicator[i][c] += (int16_t)(((float)indicator[i][c] - (float)_indicator[i][c]) / filter);
		}
	}

	// Output
	for (uint64_t i = 0; i < MATRIX_HEIGHT * MATRIX_WIDTH * 3; ++i) {
		*(page + i) = 0;
	}
	for (uint8_t i = 0; i < 9; ++i) {
		for (uint8_t c = 0; c < 3; ++c) {
			*(page + MATRIX_WIDTH * 3 - (i + 1) * 3 + c) = (uint8_t)(_indicator[8 - i][c] * brt / 255);
		}
	}
}
