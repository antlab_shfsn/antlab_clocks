/**
 * Clocks page manager for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


enum Clocks_setting { NONE_CLOCK_SET, HOUR_SET, MINUTE_SET, SECOND_SET };


// A class that represents a clocks page manager instance
class Clocks_page_manager {
	public:
		Clocks_page_manager(datetime_s init_datetime);
		void get_page(uint8_t* page, datetime_s datetime, Clocks_setting setting, uint8_t color[3], uint8_t highlight_color[3], bool prev, bool no_sec);
		uint64_t last_time_changed;
	protected:
		datetime_s _datetime;
		datetime_s __datetime;
		uint8_t _color[3];
};


// Constructor, initialization
Clocks_page_manager::Clocks_page_manager(datetime_s init_datetime) {
	last_time_changed = 0;
	_datetime = init_datetime;
	__datetime = init_datetime;
}

// Gets pointer to page, datetime_s timestamp, setting enum, color vector and optionally a flag for using prevous timestamp
// Outputs relulting page to given pointer
void Clocks_page_manager::get_page(uint8_t* page, datetime_s datetime, Clocks_setting setting, uint8_t color[3], uint8_t highlight_color[3], bool prev = false, bool no_sec = false) {
	// Check timestamp change for animation timing
	if(datetime != _datetime) {
		last_time_changed = to_us_since_boot(get_absolute_time());
		__datetime = _datetime;
		_datetime = datetime;
	}
	// Use prevous timestamp if needed
	if (prev) datetime = __datetime;
	// Clear page
	for (uint64_t i = 0; i < MATRIX_HEIGHT * MATRIX_WIDTH * 3; ++i) {
		*(page + i) = 0;
	}
	// Draw symbols
	draw_symbol(page, *nums_big[datetime.hour / 10], 0, 0, 4, 8, (setting == HOUR_SET) ? highlight_color : color);
	draw_symbol(page, *nums_big[datetime.hour % 10], 5, 0, 4, 8, (setting == HOUR_SET) ? highlight_color : color);
	draw_symbol(page, *nums_big[datetime.min / 10], 12, 0, 4, 8, (setting == MINUTE_SET) ? highlight_color : color);
	draw_symbol(page, *nums_big[datetime.min % 10], 17, 0, 4, 8, (setting == MINUTE_SET) ? highlight_color : color);
	if (!no_sec) {
		draw_symbol(page, *nums_small[datetime.sec / 10], 23, 1, 4, 7, (setting == SECOND_SET) ? highlight_color : color);
		draw_symbol(page, *nums_small[datetime.sec % 10], 28, 1, 4, 7, (setting == SECOND_SET) ? highlight_color : color);
	}
	// Draw colons
	*(page + 9 * MATRIX_WIDTH + 30) = color[0];
	*(page + 9 * MATRIX_WIDTH + 31) = color[1];
	*(page + 9 * MATRIX_WIDTH + 32) = color[2];
	*(page + 18 * MATRIX_WIDTH + 30) = color[0];
	*(page + 18 * MATRIX_WIDTH + 31) = color[1];
	*(page + 18 * MATRIX_WIDTH + 32) = color[2];
}
