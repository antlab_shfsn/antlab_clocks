/**
 * "OS" script for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */





// Initialize IO devices here:

// Screen
WS2812B screen(WS2812B_PIN, ORDER_GRB, CORNER_LEFT_TOP, DIR_VERTICAL, DIR_ZIGZAG, ROTATE_0);
// Rangefinder
HC_SR04 rangefinder(HC_SR04_TRIGGER, HC_SR04_ECHO);
//Speaker
Speaker speaker;
// Get a button instances
Button button_1(BUTTON_1_PIN);
Button button_2(BUTTON_2_PIN);
// Get a luxometer instance
Luxometer luxometer_1(LUXOMETER_1_PIN, LUXOMETER_1_AMP);
Luxometer luxometer_2(LUXOMETER_2_PIN, LUXOMETER_2_AMP);
// Get an RTC instance
RTC rtc(DS1307_SDA_PIN, DS1307_SCL_PIN);





// Global variables here:

// Input devices state
HW_states hw_states;

// Internal software state
Indicator_state indicator_state;
Clocks_state clock_state;
Timer_state timer_state;
Stopwatch_state stopwatch_state;
Alarm_state alarm_state;
SW_states sw_states;

// Pages universal instances
Page page_1, page_2, page_res, appliance_1, appliance_2, anim_image;

// Modes internal logic
extern void logic_clocks(HW_states* hw, SW_states* sw, Clocks_state* clock);
extern void logic_timer(HW_states* hw, SW_states* sw, Timer_state* timer);
extern void logic_stopwatch(HW_states* hw, SW_states* sw, Stopwatch_state* stopwatch);
extern void logic_alarm(HW_states* hw, SW_states* sw, Alarm_state* alarm);

// Mode pages drawing callers
extern void draw_page_indicator();
extern void draw_page_clocks();
extern void draw_page_timer();
extern void draw_page_stopwatch();
extern void draw_page_alarm();

// Modes composing
extern void add_to_queue(Mode_names mode, Anim_types anim, uint64_t timestamp_us);
extern void refresh_queue(uint64_t timestamp_us);





// Initialize system components here:

// Button logic with repeating events processing
void process_button_logic(Button_logic* button, uint64_t stage_2_at, uint64_t stage_2_dt, uint64_t stage_3_at, uint64_t stage_3_dt);

// Page managers
Indicator_page_manager indicator_page_manager;
Clocks_page_manager clocks_page_manager(rtc.get_datetime());	// Initial datetime determined by RTC
Clocks_page_manager timer_page_manager(timer_state.timestamp);	// Initial time reset on reboot
Clocks_page_manager stopwatch_page_manager(stopwatch_state.timestamp);	// Initial time reset on reboot
Clocks_page_manager alarm_page_manager(alarm_state.timestamp);	// Initial time reset on reboot

// Animation managers
Double_fade_anim_manager dfade_manager;
Portal_anim_manager portal_manager;
Overlap_anim_manager overlap_manager;

// Image compositor
Compositor compositor;





void setup() {
	// One-time beginning code here:

	// Queue initialization
	for (uint8_t i = 0; i < QUEUE_LEN; ++i) {
		queue_modes[i] = NONE_MODE;
		queue_anims[i] = NONE_ANIM;
		queue_timestamps[i] = 0;
	}
	queue_modes[0] = CLOCKS;
	sw_states.current_mode = queue_modes[0];
	sw_states._current_mode = queue_modes[0];
}





void get_hardware_state() {
	// Get input devices state here:

	hw_states.timestamp_us = to_us_since_boot(get_absolute_time());
	hw_states.datetime = rtc.get_datetime();

	// Rangefinder
	// Save prevous state
	hw_states._range = hw_states.range;
	hw_states.range = rangefinder.get_range();
	// Calculate interaction length in us
	if (hw_states.range != MAX_DIST_MM && hw_states._range == MAX_DIST_MM) hw_states.range_interaction_event_us = hw_states.timestamp_us;
	if (hw_states.range != MAX_DIST_MM) hw_states.range_interaction_len_us = hw_states.timestamp_us - hw_states.range_interaction_event_us;
	else hw_states.range_interaction_len_us = 0;
	// Prevent immediate reaction
	hw_states.range_active = hw_states.range_interaction_len_us > RANGE_ACTIVE_TIMEOUT_US ? true : false;

	// Button 1
	hw_states.btn_1.press = button_1.get_state();
	process_button_logic(&hw_states.btn_1, 500000, 250000, 2000000, 100000);

	// Button 2
	hw_states.btn_2.press = button_2.get_state();
	process_button_logic(&hw_states.btn_2, 500000, 250000, 2000000, 100000);

	// Luxometer
	hw_states.brightness_1 = luxometer_1.get_value();
	hw_states.brightness_2 = luxometer_2.get_value();
	hw_states.brightness = hw_states.brightness_1 > hw_states.brightness_2 ? hw_states.brightness_1 : hw_states.brightness_2;
	// Conver value to vector
	hw_states.brt_array[0] = hw_states.brightness;
	hw_states.brt_array[1] = hw_states.brightness;
	hw_states.brt_array[2] = hw_states.brightness;
}





void set_software_state() {
	// Choose what should be output here:
	
	// Remove old pages from compose queue
	refresh_queue(hw_states.timestamp_us);


	// Rangefinder controls state machine and virtual button pressing:
	// state 0 means there will be no press when in reaction (press) zone;
	// state 1 means readiness for button press event.
	//
	// Set state 0 if rangefinder is inactive prevents accidental button press on interaction start
	if (!hw_states.range_active) {
		sw_states.range_state_machine = 0;
		sw_states.btn_up.press = false;
		sw_states.btn_dn.press = false;
	}
	else {
		// Set state 1 when inside the reaction (press) zones -- get ready for mode change
		if(hw_states.range > MIN_DIST_MM + RANGE_PRESS_ZONE_MM && hw_states.range < MAX_DIST_MM - RANGE_PRESS_ZONE_MM) {
			sw_states.range_state_machine = 1;
			sw_states.btn_up.press = false;
			sw_states.btn_dn.press = false;
		}
		// "Button press" if inside reaction (press) zone at state 1
		if (sw_states.range_state_machine == 1) {
			if (hw_states.range > MAX_DIST_MM - RANGE_PRESS_ZONE_MM) sw_states.btn_up.press = true;
			else if (hw_states.range < MIN_DIST_MM + RANGE_PRESS_ZONE_MM) sw_states.btn_dn.press = true;
		}
	}


	// Virtual rangefinder buttons
	// Button up
	process_button_logic(&sw_states.btn_up, 1000000, 2000000, 7000000, 500000);
	// Button down
	process_button_logic(&sw_states.btn_dn, 1000000, 2000000, 7000000, 500000);


	// Reset settings if range detected
	if (hw_states.range_active) {
		clock_state.setting = NONE_CLOCK_SET;
		timer_state.setting = NONE_CLOCK_SET;
		alarm_state.setting = NONE_CLOCK_SET;
	}


	// Modes internal logic
	for (uint8_t mode = 0; mode != (uint8_t)NONE_MODE; ++mode) {
		switch ((Mode_names)mode) {
			case CLOCKS:
				logic_clocks(&hw_states, &sw_states, &clock_state);
				if (clock_state.setting != NONE_CLOCK_SET) {
					rtc.rtc_fetch_allowed = false;
					rtc.set_time(clock_state.timestamp);
				}
				else rtc.rtc_fetch_allowed = true;
				clock_state._setting = clock_state.setting;
				break;
			case TIMER:
				logic_timer(&hw_states, &sw_states, &timer_state);
				break;
			case STOPWATCH:
				logic_stopwatch(&hw_states, &sw_states, &stopwatch_state);
				break;
			case ALARM:
				logic_alarm(&hw_states, &sw_states, &alarm_state);
				break;
			
			default:
				break;
		}
	}


	// Mode change on virtual buttons
	if (sw_states.btn_up.event) sw_states.current_mode = (Mode_names)(((uint8_t)sw_states.current_mode + 1 + MODES_COUNT) % MODES_COUNT);
	if (sw_states.btn_dn.event) sw_states.current_mode = (Mode_names)(((uint8_t)sw_states.current_mode - 1 + MODES_COUNT) % MODES_COUNT);


	// Check interaction and return to home after specified timeout
	sw_states.home_return = false;
	if (hw_states.btn_1.press || hw_states.btn_2.press || hw_states.range_active) sw_states.last_interaction_us = hw_states.timestamp_us;
	if (hw_states.timestamp_us - sw_states.last_interaction_us > HOME_RETURN_TIMEOUT_US && !sw_states.speaker) {
		if (sw_states.current_mode == TIMER && !timer_state.running) sw_states.home_return = true;
		if (sw_states.current_mode == STOPWATCH && !stopwatch_state.running) sw_states.home_return = true;
		if (sw_states.current_mode == ALARM) sw_states.home_return = true;
	}
	if (sw_states.home_return) {
		clock_state.setting = NONE_CLOCK_SET;
		timer_state.setting = NONE_CLOCK_SET;
		alarm_state.setting = NONE_CLOCK_SET;
		sw_states.current_mode = CLOCKS;
	}


	// Manage speaker
	if (!sw_states._speaker and sw_states.speaker) sw_states.speaker_start_timestamp = hw_states.timestamp_us;
	if (hw_states.btn_1.press || hw_states.btn_2.press || hw_states.range_active) sw_states.speaker = false;
	if (hw_states.timestamp_us - sw_states.speaker_start_timestamp > SPEAKER_TURNOFF_TIMEOUT_US) sw_states.speaker = false;
	sw_states._speaker = sw_states.speaker;


	// Process modes page calls
	if (timer_state.page_call) sw_states.current_mode = TIMER;
	if (alarm_state.page_call) sw_states.current_mode = ALARM;


	// Add new mode to compose queue if mode was changed
	if (sw_states.current_mode != sw_states._current_mode) {
		add_to_queue(	sw_states.current_mode,
									((uint8_t)sw_states.current_mode > (uint8_t)sw_states._current_mode ? PORTAL_DU : PORTAL_UD),
									hw_states.timestamp_us );
		sw_states._current_mode = sw_states.current_mode;
	}
}





void compose_image() {
	// Prepare image to output here:

	// Scan queue and compose resulting image
	for (uint8_t i = 0; i < QUEUE_LEN and queue_modes[i] != NONE_MODE; ++i) {
		// Draw page
		switch (queue_modes[i]) {
			case CLOCKS:
				draw_page_clocks();
				break;
			case TIMER:
				draw_page_timer();
				break;
			case STOPWATCH:
				draw_page_stopwatch();
				break;
			case ALARM:
				draw_page_alarm();
				break;
			
			default:
				break;
		}

		// Prepare animation if needed
		switch (queue_anims[i]) {
			case DFADE_CLOCKS:
				dfade_manager.get_anim(	appliance_1.get_pointer(),
																appliance_2.get_pointer(),
																anim_image.get_pointer(),
																queue_timestamps[i],
																CLOCKS_ANIM_DURATION_US );
				break;
			case PORTAL_DU:
				portal_manager.get_anim(	appliance_1.get_pointer(),
																	appliance_2.get_pointer(),
																	anim_image.get_pointer(),
																	queue_timestamps[i],
																	MODE_CHANGE_ANIM_DURATION_US,
																	16, 10 );
				break;
			case PORTAL_UD:
				portal_manager.get_anim(	appliance_1.get_pointer(),
																	appliance_2.get_pointer(),
																	anim_image.get_pointer(),
																	queue_timestamps[i],
																	MODE_CHANGE_ANIM_DURATION_US,
																	16, -3 );
				break;
			
			default:
				break;
		}

		// Compose image
		if (queue_anims[i] != NONE_ANIM) compositor.compose(	page_1.get_pointer(), 	// revert here because page_1 is the new page
																													page_res.get_pointer(),	// and page_res is the old page
																													appliance_1.get_pointer(),
																													appliance_2.get_pointer(),
																													anim_image.get_pointer());

		// Upper drawers and compositor call draws resulting image into page_1,
		// so it must be moved into page_res for next cycle or output
		page_res.set(page_1);
	}


	// Overlap rangefinder interaction indicator
	draw_page_indicator();
	overlap_manager.get_anim(	appliance_1.get_pointer(),
														appliance_2.get_pointer(),
														anim_image.get_pointer() );
	compositor.compose(	page_res.get_pointer(),
											page_1.get_pointer(),
											appliance_1.get_pointer(),
											appliance_2.get_pointer(),
											anim_image.get_pointer());


	// Apply brightness
	page_res.mul(hw_states.brt_array);
}





void set_hardware_state() {
	// Update display, speaker, etc (if present) here:

	screen.set_image(page_res.get_pointer());
	if (sw_states.speaker) speaker.play(); else speaker.stop();
}





void process_button_logic(Button_logic* button, uint64_t stage_2_at, uint64_t stage_2_dt, uint64_t stage_3_at, uint64_t stage_3_dt) {
	// Calculate interaction length in us
	if (button->press && !button->_press) button->press_start_us = hw_states.timestamp_us;
	if (button->press) button->len_us = hw_states.timestamp_us - button->press_start_us;
	else button->len_us = 0;
	// Calculate button hold events:
	// one at 0 ms; then stage 2 and repeating stage 3
	if (!button->_len_us && button->len_us) button->event = true;
	else if (button->len_us >= stage_2_at && button->len_us < stage_3_at
						&& button->_len_us % stage_2_dt > button->len_us % stage_2_dt) button->event = true;
	else if (button->len_us >= stage_3_at
						&& button->_len_us % stage_3_dt > button->len_us % stage_3_dt) button->event = true;
	else button->event = false;
	// Save prevous states
	button->_press = button->press;
	button->_len_us = button->len_us;
}
