/**
 * Core script for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */


////////////////// A list of all the liraries available //////////////////
// NOTE: includes sequencing is important.
#include <stdio.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/util/datetime.h"
#include "hardware/irq.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"
#include "hardware/pwm.h"
#include "hardware/spi.h"
#include "hardware/i2c.h"

#include "config.h"
#include "system/io.h"

#include "devices/rtc.hpp"
#include "devices/ws2812b.hpp"
#include "devices/hc_sr04.hpp"
#include "devices/speaker.hpp"
#include "devices/button.hpp"
#include "devices/luxometer.hpp"

#include "system/symbols.hpp"
#include "system/page.hpp"
#include "system/page_managers/indicator.hpp"
#include "system/page_managers/clocks.hpp"
#include "system/anim_managers/dfade.hpp"
#include "system/anim_managers/portal.hpp"
#include "system/anim_managers/overlap.hpp"
#include "system/compositor.hpp"
#include "system/queue.hpp"
#include "system/states.hpp"
#include "system/logic.hpp"
#include "system/os.hpp"
#include "system/drawers.hpp"
//////////////////////////////////////////////////////////////////////////



////////// A wrap to make the high-level code more Arduino-like //////////

// A function that will fired once in the very beginning
extern void setup();
// A function that will get input devices data
extern void get_hardware_state();
// A function that will set internal state for image composing basing on hardware state
extern void set_software_state();
// A function that will compose the image to be output basing on software state
extern void compose_image();
// A function that will set output devices (screen, speaker, etc if present) to their calculated state
extern void set_hardware_state();
int main() {
	// Low-level initialization
	io_init();
	adc_init();
	speaker_init();
	gpio_init(LED_PIN);
	gpio_set_dir(LED_PIN, GPIO_OUT);
	// 0.4 us atomic time for WS2812B means 1/0.4us = 25 MHz switch freq and thus
	// (25/8)*4 = 12.5 MHz SPI baud rate and 0b00111100 : 0b00110000 SPI transmission bit sequencies
	spi_init(spi_default, 12500000);
	set_sys_clock_khz(CLOCK_FREQ_KHZ_NOMINAL, true);
	setup();
	// High-level loop cycle
	while (true) {
		get_hardware_state();
		set_software_state();
		compose_image();
		set_hardware_state();
	}
}
//////////////////////////////////////////////////////////////////////////
