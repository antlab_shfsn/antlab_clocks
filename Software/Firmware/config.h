/**
 * Config for Phystech.Clocks project.
 * 
 * 
 * Licence: GPLv3
 * 
 * Ivan Strebkov, MIPT, AntLab, 2024
 */



// Set USB debug outputs on / off
#define DEBUG_MODE false



// Power settings
// Normal system frequency; 176000 = 44000 * 4; 44100 Hz is a common audio bitrate
#define CLOCK_FREQ_KHZ_NOMINAL 176000



// Define LED gpio pin in case some indication will be needed
#ifdef PICO_DEFAULT_LED_PIN
#define LED_PIN PICO_DEFAULT_LED_PIN
#endif



// WS2912B matrix settings
#define MATRIX_HEIGHT 8
#define MATRIX_WIDTH 32
#define WS2812B_PIN 3
#define WS2812B_GAMMA 2.7
// Reduce massive debug outputs count
#define MAX_WS2812B_DEBUG_OUTPUTS_PER_SEC 5



// HC-SR2812B rangefinder settings
#define HC_SR04_TRIGGER 7
#define HC_SR04_ECHO 6
#define MAX_DIST_MM 250
#define MIN_DIST_MM 50
#define V_SOUND 340
// if echo is lost wait for given amount of time to recapture
#define RECAPTURE_TIME_US 1000000
#define HC_SR04_FILTER_COEF 500
// Time within which os will not react on rangefinder to prevent accidential inputs
#define RANGE_ACTIVE_TIMEOUT_US 500000
// Buffer zone for virtual "button press"
#define RANGE_PRESS_ZONE_MM 50
// Set manual values setting on / off
#define HC_SR04_MANUAL false



// Speaker settings
#define SPEAKER_PIN 28



// Buttons settings
#define BUTTON_1_PIN 8
#define BUTTON_2_PIN 9
// Set manual state setting on / off
#define BUTTON_MANUAL false    // Set manual values setting on / off



// Luxometer settings
#define LUXOMETER_1_PIN 26
#define LUXOMETER_2_PIN 27
#define LUXOMETER_1_AMP 0.785
#define LUXOMETER_2_AMP 0.8
// Set manual values setting on / off
#define LUXOMETER_MANUAL false
// Make sure that light is changed.
#define LUXOMETER_RESPONSE_TIME_MS 2000
#define LUXOMETER_UNLATCH_PERIOD_MS 30000
#define LUXOMETER_VALUE_TOL 16
#define LUXOMETER_FILTER_COEF 2500
#define LUXOMETER_MIN_BRT 63
#define LUXOMETER_GAMMA 2.7



// DS1307 RTC settings
#define DS1307_SDA_PIN 20
#define DS1307_SCL_PIN 21
#define I2C_TIMEOUT_US 1000 // If i2c blocking read/write sequence has been broken it will be interrupted after this timeout
#define RTC_VALIDATION_COUNT 256 // Number of successful i2c transactions in a row to count RTC as reliable



// OS settings
#define HOME_RETURN_TIMEOUT_US 120000000
#define SPEAKER_TURNOFF_TIMEOUT_US 300000000



// Animations settings
#define CLOCKS_ANIM_DURATION_US 750000
#define MODE_CHANGE_ANIM_DURATION_US 2500000

