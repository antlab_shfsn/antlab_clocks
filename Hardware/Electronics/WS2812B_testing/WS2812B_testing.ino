// LED strip settings
# include "microLED.h"
# include "FastLED.h"
# define COLOR_DEBTH 3
# define CRT_PGM 
# define LED_AMOUNT 33
# define LED_PIN 13
microLED < LED_AMOUNT, LED_PIN, -1, LED_WS2812, ORDER_GRB, CLI_AVER, SAVE_MILLIS > strip;


// Define pins for mode select
# define MODE_PIN 12


// Global value for led position in running light mode and color in others
uint16_t current_led = 0;
uint8_t brt = 255;
uint8_t current_color = 0;


void setup() {
  // put your setup code here, to run once:
  //Serial.begin(9600);

  // Prepare pins for mode select
  pinMode(MODE_PIN, INPUT);
}


void loop() {
  // put your main code here, to run repeatedly:

  // Get mode settings
  bool mode = false;
  if (digitalRead(MODE_PIN)) mode = false;
  else mode = true;
  if (mode) brt += 64;
  else brt = 255;

  /*Serial.print(mode);
  Serial.print(" ");
  Serial.print(current_color);
  Serial.print(" ");
  Serial.println(brt);*/


  if (!mode) {
    for (uint16_t i = 0; i < LED_AMOUNT; ++i) {
      strip.leds[i] = mRGB(0, 0, 0);
    }
    if (current_led % 3 == 0) {
      strip.leds[current_led] = mRGB(brt, 0, 0);
    } else if (current_led % 3 == 1) {
      strip.leds[current_led] = mRGB(0, brt, 0);
    } else if (current_led % 3 == 2) {
      strip.leds[current_led] = mRGB(0, 0, brt);
    }
    ++current_led;
    if (current_led >= LED_AMOUNT) current_led = 0;
    strip.show();
    delay(250);
  } else {
    uint8_t brt_r, brt_g, brt_b;
    if (current_color == 1) { brt_r = brt; brt_g = 0; brt_b = 0; }
    else if (current_color == 2) { brt_r = 0; brt_g = brt; brt_b = 0; }
    else if (current_color == 3) { brt_r = 0; brt_g = 0; brt_b = brt; }
    else { brt_r = brt; brt_g = brt; brt_b = brt; }
    for (uint16_t i = 0; i < LED_AMOUNT; ++i) {
      strip.leds[i] = mRGB(brt_r, brt_g, brt_b);
    }
    strip.show();
    if (brt == 255) current_color = (current_color + 1) % 4;
    delay(2000);
  }
}
